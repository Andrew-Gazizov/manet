/**
  ******************************************************************************
  * @file    node.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    22/11/2014
  * @brief   This file contains global variables and function prototypes for
  * 		 main network program body
  ******************************************************************************
**/

#ifndef NODE_H_
#define NODE_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */

/* Library includes. */

/* Headers includes. */
#include "misc.h"

/* Macros --------------------------------------------------------------------*/
#define ROUTING_PRIORITY	0
#define URGENT_PRIORITY		1
#define FORWARDING_PRIORITY	2
#define MY_PRIORITY 		3
#define URGENT_TC 			1

#define PACKET_LIFETIME		5

/* Global Variables -----------------------------------------------------------*/
bool CONN_STATUS;
u8 malloc_count;

/* Function Prototypes --------------------------------------------------------*/
void process_data_from_outside(byte *);
void process_chip_bad_responce(void);
void Node_Init(void);
void node_run(void);
bool write_to_outside(void);

#endif /* NODE_H_ */

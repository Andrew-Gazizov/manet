/**
  ******************************************************************************
  * @file    chip_master.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    26/05/2015
  * @brief   This file contains global variables and function prototypes for
  * 		 LAN networking
  ******************************************************************************
**/

#ifndef LAN_H_
#define LAN_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */

/* Library includes. */

/* Headers includes. */
#include "misc.h"

/* Macros --------------------------------------------------------------------*/

/* Global Variables -----------------------------------------------------------*/
int lan_skt;

/* Function Prototypes --------------------------------------------------------*/
void LAN_init(char *);
void LAN_read_and_process(void);
void LAN_write(byte *, byte *);
void LAN_write_all(byte *pkt);
bool L2toIP_is_in(byte *, unsigned int);

#endif /* LAN_H_ */

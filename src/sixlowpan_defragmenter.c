/**
  ******************************************************************************
  * @file    sixlowpan_defragmenter.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    10/12/2014
  * @brief   Provides fragmenter & defragmenter routines for 6LowPAN and it's
  * 		 managing
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Library includes. */

/* Headers includes. */
#include "sixlowpan_defragmenter.h"
#include "misc.h"
#include "binascii.h"
#include "tuntap.h"
#include "manet.h"
#include "logger.h"
#include "node.h"
#include "dynamic.h"

/* Macros --------------------------------------------------------------------*/
#define VERBOSE

/* Variables -----------------------------------------------------------------*/
unsigned short datagram_tag = 0;		// Tag for sending datagram (same for number of fragments

/* Function Prototypes -------------------------------------------------------*/
bool ap_deserialize_fragment_header(byte *, unsigned short *, unsigned short *, unsigned short *, byte *, bool *);

/**
  * @brief	void fragments_generator(byte *data, u8 max_chunk_size)
  * 		========== Generator of LoWPAN fragments
  * @param  *data: byte[x+2], byte[0:1] is message size
  * 	- Input data
  * @param  max_chunk_size: {0-255}
  * 	- Maximum fragment size
  * @retval none
*/
void fragments_generator(byte *data, u8 max_chunk_size) {
	unsigned short datagram_size = (data[1] << 8) | data[0];
	unsigned short curr_data_pos, first_chunk_octets_nm, size_of_first_chunk, chunk_octets_nm, size_of_chunk;
	byte offset=0;
	byte fragment_header[7];

	if (datagram_size > 0x7FF) printf("Too big datagram size: %i\n", datagram_size); // datagram size must fit into 11 bits
	// in fragment after addition of header must remain at least 1 octet
	if (max_chunk_size < 13) printf("max_payload_size too small for any fragment: %i\n", max_chunk_size);

	byteinit(fragment_header);
	shortappend(fragment_header, (FRAG1 << 11) | datagram_size);
	shortappend(fragment_header, datagram_tag);
	byte_swap(&fragment_header[2], 2);
	byte_swap(&fragment_header[4], 2);
	first_chunk_octets_nm = (max_chunk_size - 4) / 8;
	size_of_first_chunk = first_chunk_octets_nm * 8;
	byteadd(fragments.data[0], fragment_header);
	byteappend(fragments.data[0], &data[2], size_of_first_chunk);
	fragments.fragments_num = 1;

	curr_data_pos = size_of_first_chunk;
	chunk_octets_nm = (max_chunk_size - 5) / 8;
	size_of_chunk = chunk_octets_nm * 8;
	byteinit(fragment_header);
	shortappend(fragment_header, (FRAGN << 11) | datagram_size);
	shortappend(fragment_header, datagram_tag);
	byte_swap(&fragment_header[2], 2);
	byte_swap(&fragment_header[4], 2);

	while (curr_data_pos < datagram_size) {
		offset = curr_data_pos / 8;
		byteadd(fragments.data[fragments.fragments_num], fragment_header);
		valueappend(fragments.data[fragments.fragments_num], offset);
		if ((datagram_size - curr_data_pos) > size_of_chunk) {
			byteappend(fragments.data[fragments.fragments_num], &data[2+curr_data_pos], size_of_chunk);
		} else {
			byteappend(fragments.data[fragments.fragments_num], &data[2+curr_data_pos], datagram_size - curr_data_pos);
		}
		curr_data_pos += size_of_chunk;
		fragments.fragments_num++;
	}
	datagram_tag++;
}

//-------------------------------------------------------------------
// Accumulated Packet routines
//-------------------------------------------------------------------
#define DEFRAG_TIMEOUT 	10

Dict packets;
// Struct containing all received fragments
struct accu_pkt {
	// pkt id
	byte src[8];
	unsigned short datagram_tag;
	// ------
	unsigned short accumulated_len;
	struct {
		u8 index;
		byte chunk[128];
	} packets[16];
	u8 packets_nxtptr;
	time_t time;
};

/**
  * @brief	void accu_pkt_init(byte *pkt_id)
  * 		========== Inits new Packet Accumulator.
  * @param  *pkt_id: byte[10]
  * 	- Packet indentifier (src[8], tag[2])
  * @retval none
*/
void accu_pkt_init(byte *pkt_id) {
	struct accu_pkt *ap = malloc(sizeof(struct accu_pkt));

	memset(ap, 0, sizeof(struct accu_pkt));
	Dict_new_row(packets, pkt_id, 10, ap, sizeof(struct accu_pkt));

	free(ap);
}


/**
  * @brief	void ap_assembly_pkt(byte *output, u8 idx)
  * 		========== When all fragments is ready, assemblies full datagram
  * @param  *ap: struct accu_pkt
  * 	- Struct contains all chunks for packet assembly
  * @retval  *output: byte[x+2], byte[0:1] is message size
  * 	- Assembled datagram
*/
void ap_assembly_pkt(byte *output, struct accu_pkt *ap) {
	u8 i;

	byteinit(output);
	for (i = 0; i < ap->packets_nxtptr; i++) byteadd(output, ap->packets[i].chunk);
}

/**
  * @brief	u8 ap_add_chunk(byte* src, unsigned short tag, u8 chunk_index, byte *chunk, bool is_first)
  * 		========== Adds received datagram to packet accumulator
  * @param  *ap: struct accu_pkt
  *	  	- Packet struct from packet accumulator
  * @param  *src: byte[8]
  *	  	- Source 64bit address
  * @param  tag: {0-65535}
  * 	- Datagram tag
  * @param  idx: {0-255}
  * 	- Index of struct
  * @param chunk_index: {0-255}
  * 	- Fragment offset - octet index in packet
  * @param  *byte: byte[x+2], byte[0:1] is message size
  * 	- Input fragment
  * @retval none
*/
void ap_add_chunk(struct accu_pkt *ap, byte* src, unsigned short tag, u8 chunk_index, byte *chunk) {
	ap->accumulated_len += bytelen(chunk);
	byteinit(ap->packets[ap->packets_nxtptr].chunk);
	byteadd(ap->packets[ap->packets_nxtptr].chunk, chunk);
	ap->packets[ap->packets_nxtptr].index = chunk_index;
	ap->packets_nxtptr++;
	ap->datagram_tag = tag;
	ap->time = time(NULL);
}

/**
  * @brief	bool ap_deserialize_fragment_header(byte* raw, unsigned short *tag, unsigned short *pkt_size, unsigned short *offset,
  * 			byte *payload, bool *is_first)
  * 		========== Gets header and data from fragment
  * @param  *raw: byte[x+2], byte[0:1] is message size
  * 	- Input fragment
  * @retval *tag: {0-65535}
  * 	- Datagram tag
  * @retval  *pkt_size: {0-2047}
  * 	- Datagram size
  * @retval *offset: {0-127}
  * 	- Fragment offset
  * @retval  *payload: byte[x+2], byte[0:1] is message size
  * 	- Fragment payload
  * @retval if_first: {True, False}
  * 	- True when this is first fragment in datagram. Overwise False.
*/
bool ap_deserialize_fragment_header(byte* raw, unsigned short *tag, unsigned short *pkt_size, unsigned short *offset,
		byte *payload, bool *is_first) {
	unsigned short first_bytes = (raw[0+2] << 8) | raw[1+2];
	u8 dispatch_value = first_bytes >> 11;

	*tag = (raw[2+2] << 8) | raw[3+2];
	*pkt_size = first_bytes & 0x7FF;

	byteinit(payload);
	switch (dispatch_value) {
	case FRAG1:
		*offset = 0;
		*is_first = 1;
		byteappend(payload, &raw[2+4], ((raw[1] << 8) | raw[0]) - 4);
		break;
	case FRAGN:
		*is_first = 0;
		*offset = raw[4+2];
		byteappend(payload, &raw[2+5], ((raw[1] << 8) | raw[0]) - 5);
		break;
	default:
		printf("wrong dispatch value of fragment: %i\n", dispatch_value);
		return false;
	}
	return true;
}

//-------------------------------------------------------------------
// Defragmenter routines
//-------------------------------------------------------------------

/**
  * @brief	void Defragmenter_init(void)
  * 		========== Inits Defragmenter.
  * @param  none
  * @retval none
*/
void Defragmenter_init(void) { packets = Dict_new(); }

/**
  * @brief	u8 Defragmenter_add_fragment(byte *src, byte *data, byte *output)
  * 		========== Adds received fragment to accumulator and returns full packet when ready
  * @param  *src: byte[8]
  *	  	- Source 64bit address
  * @param  *data: byte[x+2], byte[0:1] is message size
  * 	- Binary lowpan packet without mesh and broadcast headers
  * @retval *output: byte[x+2], byte[0:1] is message size
  * 	- Full compressed IP packet when ready. Overwise empty.
  * @retval return: {IPReady, IPNotReady, False
  * 	- True when full packet assembled. False when full packet is not ready. Overwise error has occured.
*/
u8 Defragmenter_add_fragment(byte *src, byte *data, byte *output) {
	unsigned short tag, pkt_size, offset;
	bool is_first;
	byte str[32];
	byte packet_id[10];
	byte *payload = malloc(MTU+2);
	malloc_count++;
	struct accu_pkt *ap;

	if (!(ap_deserialize_fragment_header(data, &tag, &pkt_size, &offset, payload, &is_first))) {
		puts("too short fragment");
		goto err;
	}
	memcpy(packet_id, src, 8);
	memcpy(&packet_id[8], &tag, 2);

	shexify(src, str, 8);
	LOG_TRACE(DEBUG, "CHUNK: src %s, tag %i, offset %i", str, tag, offset);

	ap = Dict_get(packets, packet_id, 10);
	if (ap == NULL) {
		accu_pkt_init(packet_id);
		ap = Dict_get(packets, packet_id, 10);
	}
	ap_add_chunk(ap, src, tag, offset, payload);
	if (ap->accumulated_len > pkt_size) {
		LOG_TRACE(ERROR, "Accumulating length and pkt size mismatch. Accumulating new packet");
		Dict_del_row(packets, packet_id, 10);
		goto err;
	}
	if (ap->accumulated_len != pkt_size) goto err_not_ready;
	ap_assembly_pkt(output, ap);
	Dict_del_row(packets, packet_id, 10);
	goto out;

err:
	free(payload);
	malloc_count--;
	return false;
err_not_ready:
	free(payload);
	malloc_count--;
	return IPNotReady;
out:
	free(payload);
	malloc_count--;
	return IPReady;
}

/**
  * @brief	void Defragmenter_clean(time_t currtime)
  * 		========== Cleans obsolete fragments
  * @param  currtime: seconds from new era
  *	  	- Current time
  *	@retval none
*/
void Defragmenter_clean(time_t currtime) {
	byte packet_id[10];

	DictIterator iter = Dict_new_iterator(packets);
	struct accu_pkt *ap = Dict_iterate_values(iter);

	while (ap != NULL) {
		if ((ap->time + DEFRAG_TIMEOUT) < currtime) {
			memcpy(packet_id, ap->src, 8);
			memcpy(&packet_id[8], &ap->datagram_tag, 2);
			Dict_del_row(packets, (void *)packet_id, 10);
		}

		ap = Dict_iterate_values(iter);
	}

	Dict_delete_iterator(iter);
}



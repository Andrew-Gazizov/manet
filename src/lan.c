/**
  ******************************************************************************
  * @file    lan.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    26/05/2015
  * @brief   Provides packet exchange via UDP proto using LAN
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <arpa/inet.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

/* Library includes. */

/* Headers includes. */
#include "lan.h"
#include "node.h"
#include "p802154.h"
#include "misc.h"
#include "binascii.h"

/* Macros --------------------------------------------------------------------*/
#define LAN_PORT 			20000
#define ACTIVE 				0
#define PASSIVE				1
#define LAN_NODE_LIFETIME	100
#define LAN_BROADCAST_ADDR 	"227.0.0.1"

/* Variables -----------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/
u8 L2toIP_get_index(byte *, unsigned int);

//------------------------------------------------------------------------
// L2 to IP address tables routines
//------------------------------------------------------------------------
#define L2toIP_SIZE 16

// L2 addresses to IP addresses table
struct {
	byte l2addr[8];
	unsigned int v4addr;
	bool status;
	time_t time;
} L2toIP[L2toIP_SIZE];

u8 L2toIP_nxtptr;		// Contains next free place in L2 to IP table for writing

/**
  * @brief	void L2toIP_new(byte *l2addr, unsigned int v4addr)
  * 		========== Creates new record in L2 to IP addresses table.
  * @param  *l2addr: byte[8]
  * 	- L2 address.
  * @param	v4addr: unsigned int
  * 	- IPv4 address
  * @param	status: {ACTIVE, INACTIVE}
  * @retval none
*/
void L2toIP_new(byte *l2addr, unsigned int v4addr, bool status) {
	memcpy(L2toIP[L2toIP_nxtptr].l2addr, l2addr, 8);
	L2toIP[L2toIP_nxtptr].v4addr = v4addr;
	L2toIP[L2toIP_nxtptr].status = status;
	L2toIP[L2toIP_nxtptr].time = time(NULL);

	L2toIP_nxtptr++;
	if (L2toIP_nxtptr == L2toIP_SIZE - 1) {
		L2toIP_nxtptr = 0;
		puts("INFO: L2toIP table overwriting...");
	}
}

/**
  * @brief  bool L2toIP_time_up(unsigned int v4addr)
  * 		==========Updates time of IPv4 and L2 address accordance in L2 to IP Table
  * @param  v4addr: unsigned int
  * 	- IPv4 address for timing up
  * @retval return: {True, False}
  * 	- True when time updating is successful
  * 	- False when time updating is unsuccessful. L2 to IP Table does not contain this address.
*/
bool L2toIP_time_up(unsigned int v4addr) {
	u8 i = L2toIP_get_index(NULL, v4addr);
	if (i == 0xFF) {
		printf("ERROR: L2toIP Router table has no record with IPv4 address: %i\n ", v4addr);
		return false;
	}
	L2toIP[i].time = time(NULL);
	return true;
}

/**
  * @brief  void L2toIP_del(unsigned int v4addr)
  * 		========== Deletes L2 and IPv4 addresses link in L2toIP
  * @param  v4addr: unsigned int
  * 	- IPv4 address for deleting
*/
void L2toIP_del(unsigned int v4addr) {
	u8 i = 0;

	while (1) {
		i = L2toIP_get_index(NULL, v4addr);
		if (i == 0xFF) break;
		memset(&L2toIP[i], 0, sizeof(L2toIP[i]));
	}
}

/**
  * @brief 	bool RouteTable_is_in(byte *addr)
  * 		========== Checks whether 4 byte IPv4 or L2 address in L2toIP Table
  * @param  *l2addr: byte[8]
  * 	- L2 address for checking, or NULL when using IPv4
  * @param  v4addr: unsigned int
  * 	- IPv4 address for checking, or NULL when using L2
  * @return
*/
bool L2toIP_is_in(byte *l2addr, unsigned int v4addr) {
	if (v4addr) {
		if (L2toIP_get_index(NULL, v4addr) == 0xFF) return false;
		else return true;
	} else
	if (l2addr != NULL) {
		if (L2toIP_get_index(l2addr, 0) == 0xFF) return false;
		else return true;
	}
	return false;
}

/**
  * @brief  u8 L2toIP_get_index(unsigned int v4addr)
  * 		========== Finds record in L2 to IP addresses table that contains desired IPv4 or L2 address.
  * @param  *l2addr: byte[8]
  * 	- Desired L2 address, or NULL when using IPv4
  * @param  v4addr: unsigned int
  * 	- Desired IPv4 address, or NULL when using L2
  * @retval return: {0-255}
  * 	- Index of struct in L2 to IP addresses table (record index) that contains desired IPv4 or L2 address
*/
u8 L2toIP_get_index(byte *l2addr, unsigned int v4addr) {
	u8 i;

	if (v4addr) {
		for (i = 0; i < L2toIP_SIZE; i++) {
			if (L2toIP[i].v4addr == v4addr) return i;
			else return 0xFF;
		}
	}
	if (l2addr != NULL) {
		for (i = 0; i < L2toIP_SIZE; i++) {
			if (bytecomp(L2toIP[i].l2addr, l2addr, 8)) return i;
			else return 0xFF;
		}
	}
	return 0xFF;
}

//------------------------------------------------------------------------

/**
  * @brief	void LAN_init(byte *iface)
  * 		========== Creates and init's LAN socket for manet networking
  * @param  *lan_interface_name: byte[6]
  * 	- LAN interface name, when NULL using all available interfaces (IP 0.0.0.0)
  * @retval none
*/
void LAN_init(char *lan_interface_name) {
	char interface[32];
	char extract_ip_cmd[70+sizeof(lan_interface_name)];
	struct sockaddr_in sockaddr;
	struct ip_mreq group;
	int value;
	FILE *FP;

	if (!is_data_empty((byte *) lan_interface_name)) {
		strcpy(extract_ip_cmd, "ifconfig | grep -A 1 '");
		strcat(extract_ip_cmd, lan_interface_name);
		strcat(extract_ip_cmd, "' | tail -1 | cut -d ':' -f 2 | cut -d ' ' -f 1");

		FP = popen(extract_ip_cmd, "r");
		if (!FP) {
			puts("popen error");
			return;
		}

		while (fgets(interface, 32, FP));
		pclose(FP);

		printf("%s IP: %s \n", lan_interface_name, interface);
	} else
		strcpy(interface, "0.0.0.0");

	// sets address and port to socket structure
	sockaddr.sin_family = AF_INET;
	inet_pton(AF_INET, interface, &sockaddr.sin_addr);
	sockaddr.sin_port = htons(LAN_PORT);

	lan_skt = socket(AF_INET, SOCK_DGRAM, 0);
	if (lan_skt < 0) {
		perror("ERROR opening socket");
		exit(1);
	}

	if (bind(lan_skt, (struct sockaddr *) &sockaddr, sizeof(sockaddr)) < 0) {
		perror("binding socket");
		exit(1);
	}

	value = 0;
	if (setsockopt(lan_skt, SOL_IP, IP_MULTICAST_LOOP, (const char *) &value, sizeof(value)) < 0) {
        perror("setsockopt - IP_MULTICAST_LOOP");
        exit(1);
	}

	value = 32;
	if (setsockopt(lan_skt, SOL_IP, IP_MULTICAST_TTL, (const char *) &value, sizeof(value)) < 0) {
        perror("setsockopt - IP_MULTICAST_TTL");
        exit(1);
	}

	memset(&group, 0, sizeof(group));
	group.imr_multiaddr.s_addr = inet_addr(LAN_BROADCAST_ADDR);
	group.imr_interface.s_addr = inet_addr(interface);
	if (setsockopt(lan_skt, SOL_IP, IP_ADD_MEMBERSHIP, (char *) &group, sizeof(group)) < 0) {
		perror("setsockopt - IP_ADD_MEMBERSHIP");
        exit(1);
	}

	puts("Socket creation was successful\n");

	// Init L2 to IP address table
	L2toIP_nxtptr = 0;
	memset(L2toIP, 0, sizeof(L2toIP));
}

/**
  * @brief	void LAN_read_and_process(void)
  * 		========== Reads L2 packet from LAN interface and sends it for processing
  * @param none
  * @retval none
*/
void LAN_read_and_process(void) {
	byte pkt[512], str[32];
	unsigned short n;

	struct sockaddr_in srcaddr;
	socklen_t sendsize = sizeof(srcaddr);

	//receive a reply and print it
	//clear the buffer by filling null, it might have previously received data
	byteinit(pkt);
	n = recvfrom(lan_skt, &pkt[2], 512, 0, (struct sockaddr *) &srcaddr, &sendsize);  //first 2 bytes used for packet size

	pkt[n+2] = 10; 		// set dummy RSSI value to the end of packet
	n++;

	pkt[0] = (n & 0xFF);			// set packet size
	pkt[1] = ((n >> 8) & 0xFF);

	inet_ntop(AF_INET, &srcaddr.sin_addr.s_addr, (char *) &str, 64);
	printf("DEBUG:   <=== from LAN %s \n", str);

	if (L2toIP_is_in(NULL, srcaddr.sin_addr.s_addr))
		L2toIP_time_up(srcaddr.sin_addr.s_addr);
	else {
		P802154(pkt); 	// get L2 address from packet in p802154 struct
		L2toIP_new(p802154.src, srcaddr.sin_addr.s_addr, ACTIVE);
	}
	process_data_from_outside(pkt);
}

/**
  * @brief	void LAN_write(byte *pkt)
  * 		========== Sends L2 packet to LAN socket (lan_skt)
  * @param  *pkt: byte[x+2], byte[0:1] is message size
  * 	- L2 packet for sending via lan_skt
  * @retval none
*/
void LAN_write(byte *pkt, byte *addr) {
	struct sockaddr_in ipv4addr;
	char ipinput[INET_ADDRSTRLEN];
	byte str[32];
	u8 i = L2toIP_get_index(addr, 0);

	if (i != 0xFF) {
		ipv4addr.sin_family = AF_INET;
		ipv4addr.sin_addr.s_addr = L2toIP[i].v4addr;
		ipv4addr.sin_port = htons(LAN_PORT);
		inet_ntop(AF_INET, &L2toIP[i].v4addr, ipinput, INET_ADDRSTRLEN);
		printf("DEBUG:   ===> to LAN for %s\n", ipinput);
		if (sendto(lan_skt, &pkt[2], pkt[0], 0, (struct sockaddr *) &ipv4addr, sizeof(ipv4addr)) < 0)
			perror("LAN_write_all");
	} else {
		hexify(addr, str, 8);
		printf("ERROR: no LAN address for eui64 %s\n", str);
	}
}

/**
  * @brief	void LAN_write_all(byte *pkt)
  * 		========== Sends L2 packet to LAN socket (lan_skt)
  * @param  *pkt: byte[x+2], byte[0:1] is message size
  * 	- L2 packet for sending via lan_skt
  * @retval none
*/
void LAN_write_all(byte *pkt) {
	struct sockaddr_in ipv4addr;

	ipv4addr.sin_family = AF_INET;
	inet_pton(AF_INET, LAN_BROADCAST_ADDR, &ipv4addr.sin_addr);
	ipv4addr.sin_port = htons(LAN_PORT);

	puts("DEBUG:   ===> to LAN broadcastly");
	if (sendto(lan_skt, &pkt[2], pkt[0], 0, (struct sockaddr *) &ipv4addr, sizeof(ipv4addr)) < 0) perror("LAN_write_all");
}

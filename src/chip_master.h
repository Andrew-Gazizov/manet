/**
  ******************************************************************************
  * @file    chip_master.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    22/11/2014
  * @brief   This file contains global variables and function prototypes for
  * 		 chip serial IO
  ******************************************************************************
**/

#ifndef CHIP_MASTER_H_
#define CHIP_MASTER_H_

/* Includes ------------------------------------------------------------------*/

/* Standard includes. */
#include <unistd.h>

/* Library includes. */

/* Headers includes. */
#include "serial.h"
#include "misc.h"

/* Macros --------------------------------------------------------------------*/
// Available chip commands
#define CMD_ERROR             	0
#define CMD_SEND              	1
#define CMD_RECV              	2
#define CMD_ECHO              	3
#define CMD_GET_VER_EUI       	4
#define CMD_GET_CARRIER_SENSE	5
#define CMD_RADIO_CHANNEL     	6
#define CMD_RADIO_MODE        	7
#define CMD_TX_POWER          	8
#define CMD_RESTART           	9
#define CMD_MISC_SETTINGS     	10 // Not implemented yet
#define CMD_SNIFF_MODE        	11
#define CMD_SPEED_MODE        	12
#define CMD_ANTENNA           	13

#define RADIO_MODE_OFF        	1
#define RADIO_MODE_ON        	3

#define BAD_RESPONCE 		  	0 // 0 attemps left

#define MAX_ACK_TIMEOUT 		2

#define RADIO_FRAME_SIZE 125 // in bytes
#define WEAK_LQI_VALUE -70

/* Function Prototypes --------------------------------------------------------*/
unsigned short ChipMaster_Init(void);
void complete_init(void);
void chip_write(byte *);
void raw_write(byte, byte *);
void read_cmd_get_ver_eui(byte *);
void get_eui64(void);
void serial_reader(void);
bool is_idle(void);
bool is_timeout(void);
void turn_on_radio(void);
void turn_off_radio(void);
void get_radio_mode(void);
void restore_settings(void);

/* Global Variables ------------------------------------------------------------*/
byte eui64[8];	// Chip EUI64

#endif /* CHIP_MASTER_H_ */

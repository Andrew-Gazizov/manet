/**
  ******************************************************************************
  * @file    iptools.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    22/11/2014
  * @brief   Provides IP routines
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <arpa/inet.h>
#include <string.h>


/* Library includes. */

/* Headers includes. */
#include "iptools.h"
#include "logger.h"
#include "node.h"

/* Macros --------------------------------------------------------------------*/
#define UDP_NXT		17
#define TCP_NXT 	6
#define ICMP_NXT 	1
#define ICMPv6_NXT  58

/* Variables -----------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/

/**
  * @brief	int iptools_init(void)
  * 		========== Init IPv6 local and broadcast addresses
  * @param  none
  * @retval none
*/
void iptools_init() {
	if (!inet_pton(AF_INET6, "fe80::1", SOME_LOCAL_IP))
		LOG_TRACE(ERROR, "inet_pton error");
	memcpy(LOCAL_PREFIX, SOME_LOCAL_IP, 8);
	if (!inet_pton(AF_INET6, "ff02::1", BROADCAST_IP))
		LOG_TRACE(ERROR, "inet_pton error");

	memset(BROADCAST16, 0xFF, 8);
}

/**
  * @brief	void invert_LU_bit(byte *my_v6)
  * 		========== Inverting LU bit in EUI-64 and gets L2 address
  * @param  *my_v6: byte[8]
  *	  	- EUI-64 address
  * @retval *my_v6: byte[8]
  *	  	- L2 64 bit address
*/
void invert_LU_bit(byte *my_v6) {
	my_v6[8] ^= 2;
}

/**
  * @brief	void eui_to_ip(byte *eui64, byte *my_v6)
  * 		========== Converts EUI-64 to IPv6 address
  * @param  *eui64: byte[8]
  *	  	- EUI-64 address
  * @retval *my_v6: byte[16]
  *	  	- IPv6 address
*/
void eui_to_ip(byte *eui64, byte *my_v6) {
	memcpy(&my_v6[0], &LOCAL_PREFIX[0], 8);
	memcpy(&my_v6[8], &eui64[0], 8);
	invert_LU_bit(my_v6);

	byte str[40];
	if (!inet_ntop(AF_INET6, my_v6, (char *)str, 40))
		LOG_TRACE(ERROR, "inet_pton error");
	LOG_TRACE(INFO, "Node IPv6 address: %s", str);
}

/**
  * @brief	bool is_broadcast(byte *bin_ip)
  * 		==========Checks whether IPv6 address is a broadcast
  * @param  *bin_ip: byte[16]
  *	  	- IPv6 address
  * @retval none
*/
bool is_broadcast(byte *bin_ip) {
	return (bin_ip[0] == 0xFF);
}

/**
  * @brief	void get_l2(byte *bin_data, byte *tx_dst)
  * 		========== Converts IPv6 address to L2 64bit address
  * @param  *bin_data: byte[16]
  *	  	- IPv6 address
  * @retval *tx_dst: byte[8]
  *	  	- 64bit L2 address
*/
void get_l2(byte *bin_data, byte *tx_dst) {
	if (bin_data[0] == 0xFF) {
		memcpy(tx_dst, BROADCAST16, 8);
	}
	else	{
		tx_dst[0] = bin_data[8] ^ 2;
		memcpy(&tx_dst[1], &bin_data[9], 7);
	}
}

/**
  * @brief	void l2_to_ip(byte *bin_l2, byte *prefix, byte *ip)
  * 		========== Converts L2 64bit address to IPv6 address
  * @param  *bin_l2: byte[8]
  *	  	- 64 bit L2 address
  * @param  *prefix: byte[8]
  *	  	- IPv6 address prefix
  * @retval *ip: byte[8]
  *	  	- IPb6 address
*/
void l2_to_ip(byte *bin_l2, byte *prefix, byte *ip) {
	if (bytecomp(bin_l2, BROADCAST16, 2)) {
		memcpy(ip, BROADCAST_IP, 16);
	} else {
		memcpy(ip, prefix, 8);
		memcpy(&ip[8], bin_l2, 8);
		invert_LU_bit(ip);
	}
}

/**
  * @brief	bool l2_is_short(byte *addr)
  * 		==========Checks whether L2 address is short
  * @param  *bin_ip: byte[8]
  *	  	- 64bit L2 address
  * @retval return: {True, False}
  * 	- True when 8byte L2 address is 2 byte short. Overwise False.
*/
bool l2_is_short(byte *addr) {
	u8 i;
	for (i = 2; i < 8; i++)
		if (addr[i] != 0xFF) return false;
	return true;
}

/**
  * @brief	ip6_short_fill(byte *addr)
  * 		========== When L2 address is short fills empty space in 8byte strings by 0xFF
  * @param  *addr: byte[8]
  *	  	- 16bit L2 address
  * @retval none
*/
void ip6_short_fill(byte *addr) {
	memset(&addr[2], 0xFF, 6);
}

//-------------------------------------------------------
// IP6 Routines
//-------------------------------------------------------

/**
  * @brief	void ip6_init(byte *bin_data)
  * 		========== Fills ip6 struct
  * @param  *bin_data: byte[x+2], byte[0:1] is message size
  *	  	- Input IPv6 packet. When NULL struct fields sets to it's default values.
  * @retval none
*/
void ip6_init(byte *bin_data) {
	if (bin_data != NULL) {
		memcpy(&ip6, &bin_data[2], 40);
		byte_swap((byte *) &ip6.v_tc_flow, 4);
		byte_swap((byte *) &ip6.plen, 2);
		memcpy(ip6.data, &bin_data[42], ip6.plen);
	} else {
		ip6.v_tc_flow = 0x60000000;
		ip6.plen = 0;
		ip6.nxt = 0;
		ip6.hlim = 64;
		memcpy(ip6.src, SOME_LOCAL_IP, 8);
		memcpy(ip6.dst, SOME_LOCAL_IP, 8);
		byteinit(ip6.data);
	}
}

/**
  * @brief	unsigned char ip6_v()
  * 		========== Gets Flow Label fields from IPv6 packet (ip6 struct)
  * @retval return: {0-255}
  * 	- Flowlabel field.
*/
unsigned char ip6_v() {
	return (ip6.v_tc_flow >> 28);
}

/**
  * @brief	void ip6_v_set(unsigned char v)
  * 		========== Sets Flow Label fields from IPv6 packet (ip6 struct)
  *	@param v: {0-255}
  *		- Flowlabel field
  * @retval none
*/
void ip6_v_set(unsigned char v) {
	ip6.v_tc_flow = (ip6.v_tc_flow & ~0xF0000000) | (v << 28);
}

/**
  * @brief	unsigned int ip6_flow()
  * 		========== Gets Flow Label field from IPv6 packet (ip6 struct)
  * @retval return: {0-0xFFFFF}
  * 	- Flowlabel field.
*/
unsigned int ip6_flow() {
	return ip6.v_tc_flow & 0xFFFFF;
}

/**
  * @brief	void ip6_flow_set(unsigned int v)
  * 		========== Sets Flow Label field from IPv6 packet (ip6 struct)
  *	@param v: {0-0xFFFFF}
  *		- Flowlabel field
  * @retval none
*/
void ip6_flow_set(unsigned int v) {
	ip6.v_tc_flow = (ip6.v_tc_flow & ~0xFFFFF) |  (v & 0xFFFFF);
}

/**
  * @brief	unsigned char ip6_v()
  * 		========== Gets Flow Label fields from IPv6 packet (ip6 struct)
  * @retval return: {0-255}
  * 	- Flowlabel field.
*/
unsigned char ip6_tc() {
	return ((ip6.v_tc_flow >> 20) & 0xFF);
}

/**
  * @brief	void ip6_v_set(unsigned char v)
  * 		========== Sets Flow Label fields from IPv6 packet (ip6 struct)
  *	@param v: {0-255}
  *		- Flowlabel field
  * @retval none
*/
unsigned char ip6_tc_set(unsigned char v) {
	return (ip6.v_tc_flow & ~0x0FF00000) | (v << 20);
}

/**
  * @brief	void ip6_pack(byte *ip6_packet)
  * 		========== Packs IPv6 packet from ip6 struct
  * @retval *ip6_packet: byte[x+2], byte[0:1] is message size
  *	  	- Formed output IPv6 packet.
*/
void ip6_pack(byte *ip6_packet) {
	unsigned short plen = ip6.plen;
	byteinit(ip6_packet);
	byte_swap((byte *) &ip6.v_tc_flow, 4);
	byte_swap((byte *) &ip6.plen, 2);
	byteappend(ip6_packet, (byte *) &ip6, 40+plen+1);
}

/**
  * @brief	bool is_broadcast(byte *bin_ip)
  * 		==========Checks whether IPv6 transport is UDP. IP packet gets from ip6 struct
  * @param  none
  * @retval return: {True, False}
  * 	- True when transport is UDP. False overwise.
*/
bool ip6_is_udp() {
	return (ip6.nxt == UDP_NXT);
}

//----------------------------------------------------------
// UDP Routines
//----------------------------------------------------------

/**
  * @brief	void UDP_init(byte *bin_data)
  * 		========== Fills udp struct
  * @param  *bin_data: byte[x+2], byte[0:1] is message size
  *	  	- Input IPv6 payload. When NULL struct fields sets to it's default values.
  * @retval none
*/
void UDP_init(byte *bin_data) {
	if (bin_data != NULL) {
		memcpy(&udp, bin_data, ip6.plen);
		byte_swap((byte *) &udp.sport, 2);
		byte_swap((byte *) &udp.dport, 2);
	} else {
		udp.sport = 0xDEAD;
		udp.dport = 0;
		udp.ulen = 8;
		udp.sum = 0;
	}
}






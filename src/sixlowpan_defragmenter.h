/**
  ******************************************************************************
  * @file    sixlowpan_defragmenter.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    10/12/2014
  * @brief   This file contains global variables and function prototypes for
  * 		 6LowPAN deframenter & fragmenter
  ******************************************************************************
**/

#ifndef SIXLOWPAN_DEFRAGMENTER_H_
#define SIXLOWPAN_DEFRAGMENTER_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <time.h>

/* Library includes. */

/* Headers includes. */
#include "misc.h"


/* Macros --------------------------------------------------------------------*/
#define FRAG1 0b11000
#define FRAGN 0b11100
// Defragmenter status
#define IPReady		2	// When compressed IP accumulated
#define IPNotReady	3	// When compressed IP accumulation continues

/* Function Prototypes --------------------------------------------------------*/
void Defragmenter_init(void);
void Defragmenter_clean(time_t currtime);
u8 Defragmenter_add_fragment(byte *, byte *, byte *);
void fragments_generator(byte *, u8);

// Struct contains number of fragments for transmitting
struct {
	u8 fragments_num;
	byte data[32][256];
} fragments;

#endif /* SIXLOWPAN_DEFRAGMENTER_H_ */

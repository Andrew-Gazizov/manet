/**
  ******************************************************************************
  * @file    sixlowpan.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    26/11/2014
  * @brief   Provides 6LoWPAN packet managment routines
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <string.h>
#include <stdlib.h>

/* Library includes. */

/* Headers includes. */
#include "sixlowpan.h"
#include "sixlowpan_defragmenter.h"
#include "sixlowpan_compressor.h"
#include "iptools.h"
#include "tools.h"
#include "binascii.h"
#include "tuntap.h"
#include "node.h"
#include "misc.h"
#include "logger.h"
#include "dynamic.h"

/* Macros --------------------------------------------------------------------*/
#define BROADCAST_DB		0b01010000
#define MESH_HEADER_DB 		0b10
#define UNCOMPRESSED_IP_DB 	0b01000001
#define COMPRESSED_IP_DB 	0x42
#define MARKED_IP_DB 		0x43

/* Variables -----------------------------------------------------------------*/
byte broadcast_tag = 0;
Dict received_broadcast_packets;

/* Function Prototypes -------------------------------------------------------*/
void Fragment_Generator(byte *, bool, byte *, bool, byte);
void create_broadcast_header(byte *);
void mh_pack(byte *, byte hops_left, byte *, byte *);

/**
  * @brief  void sixlowpan_init(void)
  * 		========== Inits some internal sixlowpan routines
  *	@param  none
  * @retval none
*/
void sixlowpan_init(void) { received_broadcast_packets = SeqTable_init(); }

/**
void sixlowpan_clean(time_t currtime)
  * 		========== Clean out of date records from Sequence Table with received broadcast packets
  *	@param  currtime: seconds from new era
  *		- Current time
  * @retval none
*/
void sixlowpan_clean(time_t currtime) {	SeqTable_clean(received_broadcast_packets, currtime); }

/**
  * @brief	void LowpanGenerator(byte *packet, unsigned short max_payload_size, bool compress_ip, bool is_marked)
  * 		========== Generates LoWPAN packets from IPv6. Next binary LoWPAN packet sends to fragments struct.
  * @param  *packet: byte[x+2], byte[0:1] is message size
  * 	- Input IPv6 packet
  * @param 	max_payload_size: {1-65535}
  * 	- Maximum packet size
  * @param compress_ip: {True, False}
  * 	- True when packet must be compressed. Overwise False.
  * @param is_marked: {True, False}
  * 	- True when packet is marked (compressed only). Overwise False.
  * @retval none
*/
void LowpanGenerator(byte *packet, unsigned short max_payload_size, bool compress_ip, bool is_marked) {
	byte mesh_header[32];
	byte l2src[16];
	byte l2dst[16];

	byte *compressed_ip = malloc(MTU);
	malloc_count++;

	compress(packet, is_marked, compressed_ip);
	get_l2(ip6.src, l2src);
	get_l2(ip6.dst, l2dst);
	mh_pack(mesh_header, ip6.hlim, l2src, l2dst);
	Fragment_Generator(compressed_ip, true, mesh_header, is_broadcast((byte *)&ip6.dst), max_payload_size);

	free(compressed_ip);
	malloc_count--;
}

/**
  * @brief	MH_UNPACKER(byte *data, bool b1, bool b2, bool b3, byte *src, byte *dst)
  * 		========== Unpacks mesh header corresponding bit mask
  * @param  *data: byte[x+2], byte[0:1] is message size
  * 	- Input packet
  * @param  b1: {True, False}
  * 	- True when Hops Left more than 15. Overwise False.
  * @param  b2: {True, False}
  * 	- 64bit L2 source address is short
  * @param  b3: {True, False}
  * 	- 64bit L2 destination address is short
  * @param  *src: byte[8]
  * 	- 64bit L2 source address
  * @param  *dst: byte[8]
  * 	- 64bit L2 destination address
  * @retval none
*/
u8 MH_UNPACKER(byte *data, bool b1, bool b2, bool b3, byte *src, byte *dst) {
	switch ((b1 << 2) | (b2 << 1) | b3) {
	case 0b000:
		memcpy(src, &data[0+1], 8);
		memcpy(dst, &data[0+1+8], 8);
		return 1 + 8 + 8;
	case 0b001:
		memcpy(src, &data[0+1], 8);
		memcpy(dst, &data[0+1+8], 2);
		ip6_short_fill(dst);
		return 1 + 8 + 2;
	case 0b010:
		memcpy(src, &data[0+1], 2);
		ip6_short_fill(src);
		memcpy(dst, &data[0+1+2], 8);
		return 1 + 2 + 8;
	case 0b011:
		memcpy(src, &data[0+1], 2);
		ip6_short_fill(src);
		memcpy(dst, &data[0+1+2], 2);
		ip6_short_fill(dst);
		return 1 + 2 + 2;
	case 0b100:
		memcpy(src, &data[0+2], 8);
		memcpy(dst, &data[0+2+8], 8);
		return 2 + 8 + 8;
	case 0b101:
		memcpy(src, &data[0+2], 8);
		memcpy(dst, &data[0+2+8], 2);
		ip6_short_fill(dst);
		return 2 + 8 + 2;
	case 0b110:
		memcpy(src, &data[0+2], 2);
		ip6_short_fill(src);
		memcpy(dst, &data[0+2+2], 8);
		return 2 + 2 + 8;
	case 0b111:
		memcpy(src, &data[0+2], 2);
		ip6_short_fill(src);
		memcpy(dst, &data[0+2+2], 2);
		ip6_short_fill(dst);
		return 2 + 2 + 2;
	default:
		printf("ERROR: MH_PACKERS error: %i\n", (b1 << 2) | (b2 << 1) | b3);
		return 0;
	}
}

/**
  * void MH_PACKER(byte *data, bool b1, bool b2, bool b3, unsigned short db, byte *src, byte *dst)
  * 		========== Packs mesh header corresponding bit mask
  * @param  b1: {True, False}
  * 	- True when Hops Left more than 15. Overwise False.
  * @param  b2: {True, False}
  * 	- 64bit L2 source address is short
  * @param  b3: {True, False}
  * 	- 64bit L2 destination address is short
  * @param 	db: {0-0xFFFF}
  * 	-
  * @param  *src: byte[8]
  * 	- 64bit L2 source address
  * @param  *dst: byte[8]
  * 	- 64bit L2 destination address
  * @retval  *data: byte[x+2], byte[0:1] is message size
  * 	- Output header
*/
void MH_PACKER(byte *data, bool b1, bool b2, bool b3, unsigned short db, byte *src, byte *dst) {
	byteinit(data);
	switch ((b1 << 2) | (b2 << 1) | b3) {
	case 0b000:
		valueappend(data, (byte) db);
		byteappend(data, src, 8);
		byteappend(data, dst, 8);
		break;
	case 0b001:
		valueappend(data, (byte) db);
		byteappend(data, src, 8);
		byteappend(data, dst, 2);
		break;
	case 0b010:
		valueappend(data, (byte) db);
		byteappend(data, src, 2);
		byteappend(data, dst, 8);
		break;
	case 0b011:
		valueappend(data, (byte) db);
		byteappend(data, src, 2);
		byteappend(data, dst, 2);
		break;
	case 0b100:
		valueappend(data, db >> 8);
		valueappend(data, db & 0xFF);
		byteappend(data, src, 8);
		byteappend(data, dst, 8);
		break;
	case 0b101:
		valueappend(data, db >> 8);
		valueappend(data, db & 0xFF);
		byteappend(data, src, 8);
		byteappend(data, dst, 2);
		break;
	case 0b110:
		valueappend(data, db >> 8);
		valueappend(data, db & 0xFF);
		byteappend(data, src, 2);
		byteappend(data, dst, 8);
		break;
	case 0b111:
		valueappend(data, db >> 8);
		valueappend(data, db & 0xFF);
		byteappend(data, src, 2);
		byteappend(data, dst, 2);
		break;
	default:
		printf("ERROR: MH_PACKERS error: %i\n", (b1 << 2) | (b2 << 1) | b3);
	}
}

/**
  * u8 mh_unpack(unsigned short db, byte *raw_packet, byte *hops_left, byte *src, byte *dst)
  * 		========== Unpacks mesh header
  * @param 	db: {0-0xFFFF}
  * 	-
  * @param  *raw_packet: byte[x+2], byte[0:1] is message size
  * 	- Input packet
  * @param  hops_left: {0-255}
  * 	- Packet hops left
  * @param  *src: byte[8]
  * 	- 64bit L2 source address
  * @param  *dst: byte[8]
  * 	- 64bit L2 destination address
  * @retval return: {0-10}
  * 	- Size of mesh header
*/
u8 mh_unpack(unsigned short db, byte *raw_packet, byte *hops_left, byte *src, byte *dst) {
	bool last_bits_f = (db & 0xF) == 0xF;
	bool src_short = (db >> 5) & 0b1;
	bool dst_short = (db >> 4) & 0b1;
	u8 size;

	size = MH_UNPACKER(raw_packet, last_bits_f, src_short, dst_short, src, dst);
	if (last_bits_f) *hops_left = raw_packet[1] & 0xFF;
	else *hops_left = db & 0xF;
	return size;
}

/**
  * void mh_pack(byte *data, byte hops_left, byte *src, byte *dst)
  * 		========== Packs mesh header
  * @param  *data: byte[x+2], byte[0:1] is message size
  * 	- Packet
  * @param  hops_left: {0-255}
  * 	- Packet hops left
  * @param  *src: byte[8]
  * 	- 64bit L2 source address
  * @param  *dst: byte[8]
  * 	- 64bit L2 destination address
  * @retval none
*/
void mh_pack(byte *data, byte hops_left, byte *src, byte *dst) {
	bool last_bits_f = (hops_left >= 0xF);
	bool src_short = l2_is_short(src);
	bool dst_short = l2_is_short(dst);
	unsigned short db;

	if (last_bits_f)  db = (MESH_HEADER_DB << 14) | (src_short << 13) | (dst_short << 12) | (0xF << 8) | hops_left;
	else db = (MESH_HEADER_DB << 6) | (src_short << 5) | (dst_short << 4) | hops_left;
	MH_PACKER(data, last_bits_f, src_short, dst_short, db, src, dst);
}

/**
  * bool is_correct_not_lowpan(byte *packet)
  * 		========== Check whether the packet is LoWPAN
  * @param  *packet: byte[x+2], byte[0:1] is message size
  * 	- Packet immediately following the 802.15.4. header. Non-LoWPAN protocols that wish to coexist with LoWPAN nodes should
  * 	include a byte matching pattern 00xxxxxx immediately following the 802.15.4 header
  * @retval return: {True, False}
  * 	- True when packet is not LoWPAN. Overwise False.
*/
bool is_correct_not_lowpan(byte *packet) {
	 return ((packet[0+2] >> 6) == 0x0);
}

/**
  * bool is_compressed(byte *pkt)
  * 		========== Check whether the packet is compressed
  * @param  *pkt: byte[x+2], byte[0:1] is message size
  * 	- Input Packet
  * @retval return: {True, False}
  * 	- True when packet is compressed. Overwise False.
*/
bool is_compressed(byte *pkt) {
	return ((pkt[0+2] == COMPRESSED_IP_DB) | (pkt[0+2] == MARKED_IP_DB));
}

//---------------------------------------------------------------------------------------
//  Lowpan Routines
//---------------------------------------------------------------------------------------

/**
  * void Lowpan_init(byte *raw_packet, byte *l2src, byte *l2dst)
  * 		========== Unpacks LoWPAN packet and stores it's in lowpan struct
  * @param  *raw_packet: byte[x+2], byte[0:1] is message size
  * 	- Input LowPAN packet
  * @param  *l2src: byte[8]
  * 	- 64bit L2 source address
  * @param  *l2dst: byte[8]
  * 	- 64bit L2 destination address
  * @retval none
*/
void Lowpan_init(byte *raw_packet, byte *l2src, byte *l2dst) {
	unsigned short db;

	db = raw_packet[0+2];
	if ((db >> 5) == 0b100) {
		lowpan.mesh_len = mh_unpack(db, &raw_packet[0+2], &lowpan.hops_left, lowpan.src, lowpan.dst);
	} else {
		lowpan.hops_left = 0;
		memcpy(lowpan.src, l2src, 8);
		memcpy(lowpan.dst, l2dst, 8);
		lowpan.mesh_len = 0;
	}
	if (raw_packet[lowpan.mesh_len + 2] == BROADCAST_DB) {
		lowpan.broadcast_seq = raw_packet[lowpan.mesh_len+1+2];
		byteinit(lowpan.raw);
		byteappend(lowpan.raw, &raw_packet[lowpan.mesh_len+2+2], raw_packet[0]-lowpan.mesh_len-2);
	} else {
		lowpan.broadcast_seq = 0;
		byteinit(lowpan.raw);
		byteappend(lowpan.raw, &raw_packet[lowpan.mesh_len+2], ((raw_packet[1] << 8) | raw_packet[0]) - lowpan.mesh_len);
	}
}

/**
  * bool Lowpan_is_fragment(void)
  * 		========== Check whether the packet in lowpan struct is fragment
  * @param none
  * @retval return: {True, False}
  * 	- True when packet is fragment. Overwise False.
*/
bool Lowpan_is_fragment(void) {
	return ((lowpan.raw[0+2] >> 3) == FRAG1) | ((lowpan.raw[0+2] >> 3) == FRAGN);
}

/**
  * bool Lowpan_get_whole_payload(byte *output)
  * 		========== Assemblying full payload from fragments
  * @retval  *output: byte[x+2], byte[0:1] is message size
  * 	- Full payload
  * @retval return: {True, False}
  * 	- True when full payload is ready. Overwise False.
*/
bool Lowpan_get_whole_payload(byte *output) {
	byte broadcast_hash[9];

	if (lowpan.broadcast_seq) {
		broadcast_hash[0] = lowpan.broadcast_seq;
		memcpy(&broadcast_hash[1], lowpan.src, 8);
		if (SeqTable_in_table(received_broadcast_packets, broadcast_hash, 9)) {
			LOG_TRACE(ERROR, "DUPLICATED LOWPAN");
			return false;
		}
		SeqTable_append(received_broadcast_packets, broadcast_hash, 9);
	}
	byteinit(output);

	if (Lowpan_is_fragment()) return (Defragmenter_add_fragment(lowpan.src, lowpan.raw, output) == IPReady);

	bytecpy(output, lowpan.raw);
	return true;
}

/**
  * void Lowpan_serialize_for_retranslation(byte *data)
  * 		========== Assembling packet for next retranslation from lowpan struct
  * @retval  *data: byte[x+2], byte[0:1] is message size
  * 	- Assembed lowpan packet
*/
void Lowpan_serialize_for_retranslation(byte *data) {
	byteinit(data);
	mh_pack(data, lowpan.hops_left, lowpan.src, lowpan.dst);
	if (lowpan.broadcast_seq) {
		valueappend(data, BROADCAST_DB);
		valueappend(data, lowpan.broadcast_seq);
	}
	byteadd(data, lowpan.raw);
}
//---------------------------------------------------------------------------------------

/**
  * @brief	void Fragment_Generator(byte *data, bool use_mesh_header, byte *mesh_header, bool is_broadcast, byte max_payload_size)
  * 		========== Generates LoWPAN fragments from whole packet. Fragmets stores in fragment struct.
  * @param  *data: byte[x+2], byte[0:1] is message size
  * 	- Input whole packet packet
  * @param use_mesh_header: {True, False}
  * 	- True when packet has mesh header. Overwise False.
  * @param  *data: byte[x]
  * 	- Mesh header
  * @param is_broadcast: {True, False}
  * 	- True when packet is broadcasr. Overwise False.
  * @param 	max_payload_size: {1-65535}
  * 	- Maximum packet size
  * @retval none
*/
void Fragment_Generator(byte *data, bool use_mesh_header, byte *mesh_header, bool is_broadcast, byte max_payload_size) {
	byte prefix[64];
	byte broadcast_header[2];
	byte max_chunk_size;
	u8 i = 0;

	byteinit(prefix);
	for (i = 0; i < 32; i++) byteinit(fragments.data[i]);

	if (use_mesh_header) bytecpy(prefix, mesh_header);

	if (is_broadcast) {

		byteappend(prefix, broadcast_header, 2);
		create_broadcast_header(prefix);
	}

	max_chunk_size = max_payload_size - bytelen(prefix);

	if (bytelen(data) < max_chunk_size) {
		byteadd(fragments.data[0], prefix);
		byteadd(fragments.data[0], data);
		fragments.fragments_num = 0;
		return;
	}

	for (i = 0; i < 32; i++) {
		byteinit(fragments.data[i]);
		if (is_broadcast) create_broadcast_header(prefix);
		byteadd(fragments.data[i], prefix);
	}

	fragments_generator(data, max_chunk_size);
	return;
}

void create_broadcast_header(byte *prefix) {
	prefix[prefix[0]] = BROADCAST_DB;
	prefix[prefix[0]+1] = broadcast_tag;
	broadcast_tag++;
}

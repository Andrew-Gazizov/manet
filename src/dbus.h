/**
  ******************************************************************************
  * @file    dbus.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    19/06/2015
  * @brief   This file contains global variables and function prototypes for
  * 		 DBus manet client
  ******************************************************************************
**/

#ifndef DBUS_H_
#define DBUS_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
/* Library includes. */
/* Headers includes. */

/* Macros --------------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/
void dbus_init(void);
void dbus_method_reply(char *answer);

#endif /* DBUS_H_ */

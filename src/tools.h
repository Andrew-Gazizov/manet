/**
  ******************************************************************************
  * @file    tools.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    15/12/2014
  * @brief   This file contains global variables and function prototypes for
  * 		 Sequence Table control tool
  ******************************************************************************
**/

#ifndef TOOLS_H_
#define TOOLS_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <time.h>
/* Library includes. */

/* Headers includes. */
#include "misc.h"
#include "dynamic.h"

/* Macros --------------------------------------------------------------------*/

/* Function Prototypes --------------------------------------------------------*/
Dict SeqTable_init(void);
void SeqTable_append(Dict, byte *, u8);
bool SeqTable_in_table(Dict, byte *, u8);
time_t SeqTable_sended(Dict, byte *, u8);
void SeqTable_clean(Dict, time_t);

#endif /* TOOLS_H_ */

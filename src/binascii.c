/**
  ******************************************************************************
  * @file    binascii.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    22/11/2014
  * @brief   Provides routines for convert binary strings to ASCII strings
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <string.h>
/* Library includes. */

/* Headers includes. */
#include "binascii.h"

/* Macros --------------------------------------------------------------------*/

/* Variables -----------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/

/**
  * @brief	void hexify(byte *str, byte *hexstr, unsigned short num)
  * 		========== Converts input byte string to ASCII hex string
  * @param  *str: byte[num]
  * 	- Input byte string
  * @param  num: {1-65535}
  * 	- Number of bytes to converts
  * @retval *hexstr: byte[num]
  * 	- Converted ASCII string
*/
void hexify(byte *str, byte *hexstr, unsigned short num) {
	unsigned short i, k;
	byte hChar[2];

	k = 0;
	for (i = 0; i < num; i++) {
		if ((str[i] & 0xF) > 9) hChar[1] = (str[i] & 0xF) + 55;
		else hChar[1] = (str[i] & 0xF) + 48;
		if ((str[i] >> 4) > 9) hChar[0] = (str[i] >> 4) + 55;
		else hChar[0] = (str[i] >> 4) + 48;
		//strcat((char *) hexstr, (char *) hChar);
		hexstr[k++] = hChar[0];
		hexstr[k++] = hChar[1];
	}
}

/**
  * @brief	void hexify(byte *str, byte *hexstr, unsigned short num)
  * 		========== Converts input byte string to ASCII hex string with terminating symbol
  * @param  *str: byte[num]
  * 	- Input byte string
  * @param  num: {1-65535}
  * 	- Number of bytes to converts
  * @retval *hexstr: byte[num]
  * 	- Converted ASCII string
*/
void shexify(byte *str, byte *hexstr, unsigned short num) {
	hexify(str, hexstr, num);
	hexstr[num*2] = '\0';
}

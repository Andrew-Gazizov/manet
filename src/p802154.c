/**
  ******************************************************************************
  * @file    p802154.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    29/11/2014
  * @brief   Provides 802.15.4 packets packer and unpacker
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Library includes. */

/* Headers includes. */
#include "p802154.h"
#include "misc.h"
#include "iptools.h"
#include "chip_master.h"
#include "logger.h"
#include "dynamic.h"

/* Macros --------------------------------------------------------------------*/
#define LIFETIME_OF_L2_NEIGBOR 	5

/* Variables -----------------------------------------------------------------*/
unsigned short fcf_bank[2][2] = {{(0xCC << 8) | 0xE1, (0xCC << 8) | 0x61}, {(0xC8 << 8) | 0xC1, (0xC8 << 8) | 0x41 }};
byte my_rev_addr[8];	// My EUI64 with inverted LU
byte seq;				// Unique numbers

/* Function Prototypes -------------------------------------------------------*/

/**
  * @brief	void P802154(byte *binary)
  * 		========== Stores input L2 packet to p802154 struct
  * @param  *binary: byte[x+2], byte[0:1] is message size
  * 	- Input L2 packet
  * @retval  return: {SUCCESS, FAIL}
  * 	- Returns SUCCESS when packet parsed successfully, overwise FAIL
*/
u8 P802154(byte *binary) {
	unsigned short fcf;

	fcf = (binary[1+2] << 8) | binary[0+2];

	switch ((fcf >> 10) & 0b11) {
	case 2:
		p802154.dlen = 2;
		break;
	case 3:
		p802154.dlen = 8;
		break;
	default:
		LOG_TRACE(ERROR, "P802154 dlen error");
		return FAIL;
	}

	switch (fcf >> 14) {
	case 2:
		p802154.slen = 2;
		break;
	case 3:
		p802154.slen = 8;
		break;
	default:
		LOG_TRACE(ERROR, "P802154 slen error");
		return FAIL;
	}

	ip6_short_fill(p802154.dst);
	ip6_short_fill(p802154.src);
	p802154.seq = binary[2+2];
	memcpy(&p802154.dst[0], &binary[5+2], p802154.dlen);
	memcpy(&p802154.src[0], &binary[5+p802154.dlen+2], p802154.slen);
	byteinit(p802154.data);
	byteappend(p802154.data, &binary[5+p802154.dlen+p802154.slen+2], binary[0] - 5-p802154.dlen-p802154.slen);

	if (p802154.dlen == 8) byte_swap((byte *) p802154.dst, 8);
	else byte_swap((byte *) p802154.dst, 2);
	if (p802154.slen == 8) byte_swap((byte *) p802154.src, 8);
	else byte_swap((byte *) p802154.src, 2);
	return SUCCESS;
}

//-------------------------------------------------------------------------------
//	L2 Neighbors routines
//-------------------------------------------------------------------------------
Dict L2Neighbors;
struct l2_neighbor {
	byte addr[8];
	u8 seq;
	time_t time;
};

/**
  * @brief	void L2Neighbors_init(void)
  * 		========== Inits L2 Neighbor record table
  * @param  none
  * @retval none
*/
void L2Neighbors_init(void) { L2Neighbors = Dict_new(); }

/**
  * @brief	void L2Neighbors_append(byte *addr, u8 seq)
  * 		========== Appends L2 Neighbor record
  * @param  *addr: byte[8]
  * 	- Originator 64bit address.
  * @param  seq: {0-255}
  * 	- Record number
  * @retval none
*/
void L2Neighbors_append(byte *addr, u8 seq) {
	struct l2_neighbor l;
	memcpy(l.addr, addr, 8);
	l.seq = seq;
	l.time = time(NULL);
	Dict_new_row(L2Neighbors, addr, 8, &l, sizeof(struct l2_neighbor));
}

/**
  * @brief	void L2Neighbors_del(byte *addr)
  * 		========== Deletes L2 Neighbor record
  * @param  *addr: byte[8]
  * 	- Originator 64bit address.
  * @retval none
*/
void L2Neighbors_del(byte *addr) { Dict_del_row(L2Neighbors, addr, 8); }

/**
  * @brief  void L2Neighbors_clean(time_t currtime)
  * 		========== Clean out of date neighbours
  *	@param  currtime: seconds from new era
  *		- Current time
  * @retval none
*/
void L2Neighbors_clean(time_t currtime) {
	DictIterator iter = Dict_new_iterator(L2Neighbors);
	struct l2_neighbor *l = Dict_iterate_values(iter);

	while (l != NULL) {
		if (currtime - l->time > LIFETIME_OF_L2_NEIGBOR) Dict_del_row(L2Neighbors, l->addr, 8);
		l = Dict_iterate_values(iter);
	}
	Dict_delete_iterator(iter);
}

//-------------------------------------------------------------------------------
//	L2 Master routines
//-------------------------------------------------------------------------------

/**
  * @brief	void L2Master(byte *my_addr, u8 l2_lifetime)
  * 		========== Inits L2 packer & unpacker
  * @param  *my_addr: byte[8]
  * 	- My L2 address
  * @param l2_lifetime: {1-255}
  * 	- L2 packet lifetime
  * @retval  none
*/
void L2Master(byte *my_addr, u8 l2_lifetime) {
	byte_swap_tofrom(my_rev_addr, my_addr, 8);
	seq = 0;
	L2Neighbors_init();
}

/**
  * @brief	u8 get_seq(void)
  * 		========== Gets next value in random sequence
  * @param  none
  * @retval  return: {0-0xFF}
  * 	- Random number
*/
u8 get_seq(void) {
	return seq++;
}

/**
  * @brief	void l2pack(byte *dst, byte *payload, bool priority, byte *output)
  * 		========== Packs L2 packet from payload
  * @param  *src: byte[8]
  * 	- 64bit L2 source address
  * @param  *dst: byte[8]
  * 	- 64bit L2 destination address
  * @param  *payload: byte[x+2], byte[0:1] is message size
  * 	- L2 packet payload
  * @param priority: {True, False}
  * 	- True when L2 packet has high priority. Overwise False.
  * @retval  *output: byte[x+2], byte[0:1] is message size
  * 	- Packed L2 packet
*/
void l2pack(byte *src, byte *dst, byte *payload, bool priority, byte *output) {
	bool dst_mode;
	unsigned short fcf;
	byte swap_dst[8];
	byte swap_src[8];

	if (l2_is_short(dst)) dst_mode = 1;
	else dst_mode = 0;

	fcf = fcf_bank[dst_mode][priority];
	byteinit(output);
	byteappend(output, (byte *) &fcf, 2);
	if (priority) valueappend(output, get_seq());
	else valueappend(output, p802154.seq);
	valueappend(output, 0xFF);
	valueappend(output, 0xFF);

	if (dst_mode) {
		byte_swap_tofrom(swap_dst, dst, 2);
		byteappend(output, swap_dst, 2);
	} else {
		byte_swap_tofrom(swap_dst, dst, 8);
		byteappend(output, swap_dst, 8);
	}

	byte_swap_tofrom(swap_src, eui64, 8);
	byteappend(output, swap_src, 8);
	byteadd(output, payload);
}

/**
  * @brief	void l2unpack(byte *binary)
  * 		========== Unpacks input L2 packet
  * @param  *binary: byte[x+2], byte[0:1] is message size
  * 	- Input L2 packet
  * @retval  none
*/
u8 l2unpack(byte *binary) {
	if (P802154(binary) == FAIL) return FAIL;

	struct l2_neighbor *l = Dict_get(L2Neighbors, p802154.src, 8);
	if (l == NULL) {
		L2Neighbors_append(p802154.src, p802154.seq);
		return SUCCESS;
	} else {
		if (l->seq == p802154.seq) {
			LOG_TRACE(ERROR, "duplicated L2 packet");
			return DUP_L2_PKT;
		}
		l->seq = p802154.seq;
		l->time = time(NULL);
	}

	return SUCCESS;
}






/**
  ******************************************************************************
  * @file    tools.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    15/12/2014
  * @brief   Provides Sequence Table control tool
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <string.h>
#include <stdlib.h>

/* Library includes. */

/* Headers includes. */
#include "tools.h"
#include "binascii.h"
#include "dynamic.h"

/* Defines --------------------------------------------------------------------*/
#define SEQ_MAXTIME 	1

/* Function Prototypes --------------------------------------------------------*/

//-----------------------------------------------------------------------------
// Sequence Table Routines
//-----------------------------------------------------------------------------
#define MAX_SEQTABLEKEY_SIZE	16

// Sequence
struct seq {
	byte key[MAX_SEQTABLEKEY_SIZE];			// Sequence
	unsigned int size;
	time_t time;						// Sequence time of creation in seconds from new era
};

/**
  * @brief  SeqTable_init()
  * 		========== Inits Sequence Table.
  * @param  none
  * @retval return Dict seq_table
  * 	- Pointer to new instance of tabke
*/
Dict SeqTable_init(void) { return Dict_new(); }

/*
  * @brief	SeqTable_append(byte *key)
  * 		========== Appends new record in Sequence Table.
  * @param Dict seq_table
  * 	- Pointer to table thats we must append new key
  * @param  *key: byte[8]
  * 	- Storing key
  * @param size: {0-SEQTABLEKEY_SIZE}
  * 	- Size of storing key
  * @retval none
*/
void SeqTable_append(Dict seq_table, byte *key, u8 size) {
	struct seq *s = malloc(sizeof(struct seq));
	memcpy(s->key, key, size);
	s->size = size;
	s->time = time(NULL);
	Dict_new_row(seq_table, key, size, s, sizeof(struct seq));
	free(s);
}

/*
  * @brief	SeqTable_in_table(byte *key, u8 size)
  * 		========== Checks record in Sequence Table.
  * @param Dict seq_table
  * 	- Pointer to table where must find record
  * @param  *key: byte[8]
  * 	- Checking key
  * @param size: {0-SEQTABLEKEY_SIZE}
  * 	- Size of Cheking key
  * @retval return: {true, false}
  * 	- Returns True when desired key present in sequence table
*/
bool SeqTable_in_table(Dict seq_table, byte *key, u8 size) {
	if (Dict_get(seq_table, key, size) == NULL)	return false;
	else return true;
}

/*
  * @brief	time_t SeqTable_sended(byte *key, u8 size)
  * 		========== Gets create time of record in Sequence Table.
  * @param Dict seq_table
  * 	- Pointer to table
  * @param  *key: byte[8]
  * 	- Key
  * @param  size: {0-SEQTABLEKEY_SIZE}
  * 	- Size of key
  * @retval return: in seconds from new era
  * 	- Create time of record in Sequence Table. Return 0 when record is not found
*/
time_t SeqTable_sended(Dict seq_table, byte *key, u8 size) {
	struct seq *s = Dict_get(seq_table, key, size);
	if (s == NULL) return 0;

	return s->time;
}

/**
  * @brief  void SeqTable_del(byte *key, size)
  * 		========== Delete record in Sequence Table
  * @param Dict seq_table
  * 	- Pointer to table where must find record and delete it
  * @param  *key: byte[8]
  * 	- Key
  * @param  size: {0-SEQTABLEKEY_SIZE}
  * 	- Size of key
  * @retval none
*/
void SeqTable_del(Dict seq_table, byte *key, u8 size) {	Dict_del_row(seq_table, key, size); }

/**
  * @brief  void SeqTable_clean(time_t now)
  * 		========== Clean out of date records from Sequence Table
  * @param Dict seq_table
  * 	- Pointer to table thats must be cleaned
  *	@param  now: seconds from new era
  *		- Current time
  * @retval none
*/
void SeqTable_clean(Dict seq_table, time_t now) {
	DictIterator iter = Dict_new_iterator(seq_table);
	struct seq *s;
	s = Dict_iterate_values(iter);

	while (s != NULL) {
		if ((now - s->time) > SEQ_MAXTIME)
			SeqTable_del(seq_table, s->key, s->size);
		s = Dict_iterate_values(iter);
	}
	Dict_delete_iterator(iter);
}

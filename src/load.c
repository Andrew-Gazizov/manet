/**
  ******************************************************************************
  * @file    load.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    03/12/2014
  * @brief   Provides LOAD routing protocol and tables
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <stdlib.h>
/* Library includes. */

/* Headers includes. */
#include "load.h"
#include "p802154.h"
#include "chip_master.h"
#include "iptools.h"
#include "node.h"
#include "tools.h"
#include "manet.h"
#include "binascii.h"
#include "misc.h"
#include "queue.h"
#include "logger.h"
#include "dynamic.h"

/* Macros --------------------------------------------------------------------*/
//Route status
#define VALID 			1
#define INVALID 		2
#define ROUTE_DISCOVERY 3

#define MAX_RC	4
#define WL 		0
#define RC		1

#define RREQ_RATELIMIT	3
#define RERR_RATELIMIT 	3

#define RREQ_RATELIMIT_PAUSE 	1
#define RERR_RATELIMIT_PAUSE 	1
#define ROUTE_LIFETIME	 		60
#define ROUTEREQ_LIFETIME	 	5

#define RERR_Error0  0x00 // No available route
#define RERR_Error1  0x01 // Low battery
#define RERR_Error2  0x02 // routing cost not supported
#define RERR_Error3  0x03 // 0x03 - 0xff = reserved (TBD)

//#define VERBOSE_TABLES
//#define VERBOSE

/* Function Prototypes --------------------------------------------------------*/
bool RouteTable_del(byte *);
void RouteTable_new(byte *, byte *, u8);
void ReqTable_del(byte, byte *);

/* Variables ------------------------------------------------------------------*/
Dict sended_rreq;
Dict sended_rerr;


// Route Request and Reply messages form
struct {
	unsigned char type;	// 1 is a RREQ message, 2 is a RREP message.
	unsigned char r;	// 1 Local Repair
	unsigned char d;	// 1 for the 16 bit, 0 for the EUI-64 address. Must be 0.
	unsigned char o;	// 1 for the 16 bit, 0 for the EUI-64 address. Must be 0.
	unsigned char ct;	// Hop count while avoiding weak links
	unsigned char wl;	// The total number of weak links on the routing path
	unsigned char id;	// A sequence number
	unsigned char rc;  	// The accumulated link cost
	byte dst[8];		// EUI-64 link layer address of the destination
	byte orig[8];		// EUI-64 link layer address of the originator
} Rreqp_hdr;

// Route Error message form
struct {
	unsigned char type;		// 1 is a RREQ message, 2 is a RREP message.
	unsigned char d;		// 1 for the 16 bit, 0 for the EUI-64 address. Must be 0.
	unsigned char error;	// Error: 0=No available route, 1=Low battery, 2=Routing cost no supported
	byte unreach[8];		// EUI-64 link layer address of the destination
	byte orig[8];			// EUI-64 link layer address of the originator
} Rerr_hdr;

/**
  * @brief	void Rreqp(byte *data)
  * 		========== Saves recieved Route Request/Reply message to Rreqp_hdr struct
  * @param  *data: byte[24+2], byte[0:1] is message size
  * 	- Input Route Request/Reply message
  * @retval none
*/
void Rreqp(byte *data) {
	memcpy(&Rreqp_hdr, &data[0+2], sizeof(Rreqp_hdr));
}

/**
  * @brief	void Rerr(byte *data)
  * 		========== Saves recieved Route Error message to Rerr_hdr struct
  * @param  *data: byte[24+2], byte[0:1] is message size
  * 	- Input Route Error message
  * @retval none
*/
void Rerr(byte *data) {
	memcpy(&Rerr_hdr, &data[0+2], sizeof(Rerr_hdr));
}

/**
  * @brief	void Rreq_gen(byte *dst64, byte *rreq)
  * 		========== Generates Route Request message from Rreqp_hdr struct
  * @param  *dst64: byte[8]
  * 	- Route Request destination 64bit address
  * @retval *rreq: byte[24+2], byte[0:1] is message size
  * 	- Route Request message
*/
void Rreq_gen(byte *dst64, byte *rreq) {
	time_t curtm = time(NULL);

	byteinit(rreq);
	if (!SeqTable_in_table(sended_rreq, dst64, 8) || curtm >=
			(SeqTable_sended(sended_rreq, dst64, 8) + RREQ_RATELIMIT_PAUSE)) {
		Rreqp_hdr.type = 1;
		Rreqp_hdr.id++;
		memcpy(Rreqp_hdr.orig, eui64, 8);
		memcpy(Rreqp_hdr.dst, dst64, 8);
		byteappend(rreq, (byte *) &Rreqp_hdr, sizeof(Rreqp_hdr));
		SeqTable_append(sended_rreq, dst64, 8);
	} else {
		free(rreq);
		rreq = NULL;
	}
}

/**
  * @brief	void Rerr_gen(byte *unreach, byte *orig, byte *rerr)
  * 		========== Generates Route Error message from Rerr_hdr struct
  * @param  *unreach: byte[8]
  * 	- Route unreachable destination 64bit address
   * @param  *orig: byte[8]
  * 	- Route message originator 64bit address
  * @retval *rerr: byte[19+2], byte[0:1] is message size
  * 	- Route Error message
*/
void Rerr_gen(byte *unreach, byte *orig, byte *rerr) {
	time_t curtm = time(NULL);

	byteinit(rerr);
	if (!SeqTable_in_table(sended_rerr, orig, 8) || curtm >=
			(SeqTable_sended(sended_rerr, orig, 8) + RERR_RATELIMIT_PAUSE)) {
		memcpy(Rerr_hdr.unreach, unreach, 8);
		memcpy(Rerr_hdr.orig, orig, 8);
		byteappend(rerr, (byte *) &Rerr_hdr, sizeof(Rerr_hdr));
		SeqTable_append(sended_rerr, orig, 8);
	} else {
		free(rerr);
		rerr = NULL;
	}
}

/**
  * @brief  void Rrep_gen(byte *rrep)
  * 		========== Generates Route Reply message from Rreqp_hdr struct
  * @retval *rrep: byte[24+2], byte[0:1] is message size
  * 	- Route Reply message
*/
void Rrep_gen(byte *rrep) {
	byteinit(rrep);
	Rreqp_hdr.type = 2;
	byteappend(rrep, (byte *) &Rreqp_hdr, sizeof(Rreqp_hdr));
}

/**
  * @brief	bool Rreqp_fwd(byte *pkt)
  * 		========== Generates Route Request/Reply message for retranslation from Rreqp_hdr struct
  * @param  none
  * @retval *pkt: byte[24+2], byte[0:1] is message size
  * 	- Route Request/Reply message for retranslation. If max route cost achieved and packet must be killed
  * 		this message is empty
*/
void Rreqp_fwd(byte *pkt) {
	byteinit(pkt);
	Rreqp_hdr.rc++;
	if (Rreqp_hdr.rc > MAX_RC) return;
	byteappend(pkt, (byte *) &Rreqp_hdr, sizeof(Rreqp_hdr));
}

//------------------------------------------------------------------------
// Route Table routines
//------------------------------------------------------------------------
Dict route_table;

struct route {
	byte dst[8];		// Destination address
	byte nxt[8];		// Next hop for destination reach
	u8 status;			// Current route status
	time_t time;		// Update time of this route in seconds
};


/**
  * @brief  RouteTable_init()
  * 		========== Inits Route Table. Add Broadcast Route to the Route Table
  * @param  none
  * @retval none
*/
void RouteTable_init() {
	route_table = Dict_new();

	// Add permanent broadcast address to table
	byte addr[8];
	memset(addr, 0xFF, 8);
	RouteTable_new(addr, addr, VALID);
}

/**
  * @brief	void RouteTable_new(byte *dst, byte *nxt, u8 status)
  * 		========== Creates new record in Route Table.
  * @param  *dst: byte[8]
  * 	- Destination 64bit address.
  * @param	*nxt: byte[8]
  * 	- Next hop 64bit address
  * @param	status: {VALID, INVALID, ROUTE_DISCOVERY}
  * @retval none
*/
void RouteTable_new(byte *dst, byte *nxt, u8 status) {
	RouteTable_del(dst);

	struct route *r = malloc(sizeof(struct route));
	memcpy(r->dst, dst, 8);
	memcpy(r->nxt, nxt, 8);
	r->status = status;
	r->time = time(NULL);

	Dict_new_row(route_table, dst, 8, r, sizeof(struct route));
	free(r);
}

/**
  * @brief	bool RouteTable_is_neib(byte *dst)
  * 		========== Checks whether destination address is neighboring node address
  * @param  *dst: Pointer to byte array containing destination 8 byte address.
  * @retval return: {true, false}
*/
bool RouteTable_is_neib(byte *dst) {
	struct route *r = Dict_get(route_table, dst, 8);
	if (r == NULL) return false;

	if (bytecomp(r->nxt, dst, 8)) return true;
	else return false;

}

/**
  * @brief  bool RouteTable_time_up(byte *dst)
  * 		==========Updates time of route to destination address of Route Table
  * @param  *dst: Pointer to byte array containing destination 8 byte address.
  * @retval return: {true, false}
  * 	- true when time updating is successful
  * 	- false when time updating is unsuccessful. Route Table does not contain this address.
*/
bool RouteTable_time_up(byte *dst) {
	struct route *r = Dict_get(route_table, dst, 8);
	if (r == NULL) {
		return false;
		LOG_TRACE(ERROR, "RouterTable_time_up Router table has no record with orig: %s\n ", dst);
	}

	r->time = time(NULL);
	return true;
}

/**
  * @brief  void RouteTable_get_next_hop(byte *dst, byte *hop)
  * 		========== Gets next hop address for reaching destination address node
  * @param  *dst: byte[8]
  * 	- Destination 64bit address
  * @retval	*hop: byte[8]
  * 	- Next hop 64bit address for reaching destination
  * @retval return: { true, false }
  * 	- True when hop was read successfully, overwise False.
*/
bool RouteTable_get_next_hop(byte *dst, byte *hop) {
	struct route *r = Dict_get(route_table, dst, 8);
	if (r == NULL) {
		return false;
		LOG_TRACE(ERROR, "RouteTable_get_next_hop Router table has no record with orig: %s\n ", dst);
	}

	memcpy(hop, r->nxt, 8);
	return true;
}

/**
  * @brief  void RouteTable_replace_route(byte *dst, byte *nxt)
  * 		========== Replaces route in Route Table with new next hop address.
  * @param  *dst: byte[8]
  * 	- Destination 64bit address
  * @param	*hop: byte[8]
  * 	- Next hop 64bit address for reaching destination
  * @retval return: { true, false }
  * 	- True when route replaced successfully, overwise False.
*/
bool RouteTable_replace_route(byte *dst, byte *nxt) {
	struct route *r = Dict_get(route_table, dst, 8);
	if (r == NULL) {
		return false;
		LOG_TRACE(ERROR, "RouteTable_replace_route Router table has no record with orig: %s\n ", dst);
	}

	memcpy(r->nxt, nxt, 8);
	r->status = VALID;
	r->time = time(NULL);
	return true;
}

/**
  * @brief  void RouteTable_del(byte *dst)
  * 		========== Delete route in Route Table
  * @param  *dst: byte[8]
  * 	- Destination 64bit address
  * @retval return: { true, false }
  * 	- True when route deletd successfully, overwise False.
*/
bool RouteTable_del(byte *dst) { return Dict_del_row(route_table, dst, 8); }

/**
  * @brief  void RouteTable_clean(time_t ctime)
  * 		========== Clean out of date routes from Route Table
  *	@param  ctime: seconds from new era
  *		- Current time
  * @retval none
*/
void RouteTable_clean(time_t ctime) {
	DictIterator iter = Dict_new_iterator(route_table);
	struct route *r = Dict_iterate_values(iter);		// first iteration is broadcast addre4ss, skip it!
	r = Dict_iterate_values(iter);

	while (r != NULL) {
		if ((ctime - r->time) > ROUTE_LIFETIME)
			RouteTable_del(r->dst);
		r = Dict_iterate_values(iter);
	}

	Dict_delete_iterator(iter);
}

/**
  * @brief 	bool RouteTable_is_in(byte *addr)
  * 		========== Checks whether 8 byte destination address in Router Table
  * @param  none
  * @retval return: { true, false }
  * 	- True when route present in route table successfully, overwise False.
*/
bool RouteTable_is_in(byte *addr) {
	if (Dict_get(route_table, addr, 8) == NULL) return false;
	else return true;
}

/**
  * @brief	void Router_print(void)
  *  		========== Prints current Router Table
  * @param  none
  * @retval none
*/
void Router_print(void) {
	DictIterator iter = Dict_new_iterator(route_table);
	struct route *r;
	byte str[17];
	r = Dict_iterate_values(iter);
	str[16] = '\0';

	puts("\n-------------------RouteTable------------------");
	puts("|   Destination  |    Next hop    |Status|Time|");
	while (r != NULL) {
		if (r->time) {
			hexify(r->dst, str, 8); 	printf(" %s ", str);
			hexify(r->nxt, str, 8); printf("%s", str);
			switch (r->status) {
			case VALID:
				printf(" %s", "VALID");
				break;
			case INVALID:
				printf(" %s", "INVAL");
				break;
			case ROUTE_DISCOVERY:
				printf(" %s", "RDISC");
				break;
			default:
				printf(" %s", "?????");
			}
			printf("   %i\n", r->nxt[0] == 0xFF ? 0 : (int) (time(NULL) - r->time)); // If broadcast do not dislpay time
		}
		r = Dict_iterate_values(iter);
	}
	Dict_delete_iterator(iter);
}

//---------------------------------------------------------------------------------

//---------------------------------------------------------------------------------
// Req Table Routines
//---------------------------------------------------------------------------------
Dict req_table;

// Request
struct req {
	byte id;
	byte orig[8];
	byte revA[8];
	u8 fRC[2];		// 0 - wl; 1 - rc.
	u8 rRC[2];		// 0 - wl; 1 - rc.
	time_t time;
};

/**
  * @brief  RequestTable_init()
  * 		========== Inits Request Table.
  * @param  none
  * @retval none
*/
void ReqTable_init() { req_table = Dict_new(); }

/**
  * @brief	void ReqTable_new(byte id, byte *orig, byte *revA, u8 *fRC)
  * 		========== Creates new record in Request Table.
  *	@param  id: {0-255}
  *		- Route Request ID
  * @param  *orig: byte[8]
  * 	- Originator 64bit address.
  * @param	*revA: byte[8]
  * 	- Reverse route 64bit address
  * @param	fRC: byte[2]
  * 	- Forward route cost. fRC[WL] is number of weak links on routing path.
  * 						  fRC[RC] is total hops on routing path.
  * @retval none
*/
void ReqTable_new(byte id, byte *orig, byte *revA, u8 *fRC) {
	ReqTable_del(id, orig);
	struct req *r = malloc(sizeof(struct req));

	r->id = id;
	memcpy(r->orig, orig, 8);
	memcpy(r->revA, revA, 8);
	r->fRC[WL] = fRC[WL];
	r->fRC[RC] = fRC[RC];
	r->rRC[WL] = MAX_RC;
	r->rRC[RC] = MAX_RC;
	r->time = time(NULL);
	// In req tables as key used first 9 bytes of struct req (id + orig)
	Dict_new_row(req_table, r, 9, r, sizeof(struct req));
	free(r);
}

/**
  * @brief	ReqTable_change(byte id, byte *orig, byte *revA, u8 *fRC, u8 *rRC)
  * 		========== Change existing record in Request Table.
  *	@param  id: {0-255}
  *		- Route Request ID
  * @param  *orig: byte[8]
  * 	- Originator 64bit address.
  * @param	*revA: byte[8]
  * 	- NULL when change not needed
  * 	- New Reverse route 64bit address
  * @param	fRC: byte[2]
  * 	- NULL when change not needed
  * 	- Forward route cost. fRC[WL] is number of weak links on routing path.
  * 						  fRC[RC] is total hops on routing path.
   @param	rRC: byte[2]
  * 	- NULL when change not needed
  * 	- Reverse route cost. rRC[WL] is number of weak links on routing path.
  * 						  rRC[RC] is total hops on routing path.
  * @retval return: { true, false }
  * 	- False when record with desired id is not exist
*/
bool ReqTable_change(byte id, byte *orig, byte *revA, u8 *fRC, u8 *rRC) {
	byte rId[9];
	rId[0] = id;
	memcpy(&rId[1], orig, 8);

	struct req *r = Dict_get(req_table, rId, 9);
	if (r == NULL) return false;

	if (revA != NULL) memcpy(r->revA, revA, 8);
	if (fRC != NULL) memcpy(r->fRC, fRC, 2);
	if (rRC != NULL) memcpy(r->rRC, rRC, 2);
	r->time = time(NULL);

	return true;
}

/**
  * @brief	bool ReqTable_cmp_fRC(byte id, byte *orig, u8 *fRC)
  * 		========== Comparing new Forward Route Cost with existing record in Request Table.
  *	@param  id: {0-255}
  *		- Route Request ID
  * @param  *orig: byte[8]
  * 	- Originator 64bit address.
  * @param	fRC: byte[2]
  * 	- Forward route cost. fRC[WL] is number of weak links on routing path.
  * 						  fRC[RC] is total hops on routing path.
  * @retval return: {true, false}
  * 	- true when new Forward Route cost is better than existing in Request Table
  * 	- false when new Forward Route cost is worse than existing in Request Table
*/
bool ReqTable_cmp_fRC(byte id, byte *orig, u8 *fRC) {
	byte rId[9];
	rId[0] = id;
	memcpy(&rId[1], orig, 8);

	struct req *r = Dict_get(req_table, rId, 9);
	if (r == NULL) return true;

	return ((fRC[WL] < r->fRC[WL]) || ((fRC[WL] == r->fRC[WL]) && (fRC[RC] < r->fRC[RC])));
}

/**
  * @brief	bool ReqTable_cmp_rRC(byte id, byte *orig, u8 *fRC)
  * 		========== Comparing new Reverse Route Cost with existing record in Request Table.
  *	@param  id: {0-255}
  *		- Route Request ID
  * @param  *orig: byte[8]
  * 	- Originator 64bit address.
  * @param	rRC: byte[2]
  * 	- Reverse route cost. fRC[WL] is number of weak links on routing path.
  * 						  fRC[RC] is total hops on routing path.
  * @retval return: {true, false}
  * 	- true when new Reverse Route cost is better than existing in Request Table
  * 	- false when new Reverse Route cost is worse than existing in Request Table
*/
bool ReqTable_cmp_rRC(byte id, byte *orig, u8 *rRC) {
	byte rId[9];
	rId[0] = id;
	memcpy(&rId[1], orig, 8);

	struct req *r = Dict_get(req_table, rId, 9);
	if (r == NULL) return true;

	return ((rRC[0] < r->rRC[0]) | ((rRC[0] == r->rRC[0]) & (rRC[1] < r->rRC[1])));
}

/**
  * @brief	void ReqTable_upd_rRC(byte id, byte *orig, u8 *rRC)
  * 		========== Updates new Reverse Route Cost value in existing record in Request Table.
  *	@param  id: {0-255}
  *		- Route Request ID
  * @param  *orig: byte[8]
  * 	- Originator 64bit address.
  * @param	rRC: byte[2]
  * 	- New reverse Route Cost value. fRC[WL] is number of weak links on routing path.
  * 						            fRC[RC] is total hops on routing path.
  * @retval return: { true, false }
  * 	- False when record with desired id is not exist, ovewise True
*/
bool ReqTable_upd_rRC(byte id, byte *orig, u8 *rRC) {
	byte rId[9];
	rId[0] = id;
	memcpy(&rId[1], orig, 8);

	struct req *r = Dict_get(req_table, rId, 9);
	if (r == NULL) return false;

	memcpy(r->rRC, rRC, 2);
	r->time = time(NULL);
	return true;
}

/**
  * @brief	void ReqTable_get_revA(byte id, byte *orig, byte *revA)
  * 		========== Gets Reverse Address from Request table.
  *	@param  id: {0-255}
  *		- Route Request ID
  * @param  *orig: byte[8]
  * 	- Originator 64bit address.
  * @retval *revA: byte[8]
  * 	- Reverse route 64bit address in Route Request table
  * @retval return: { true, false }
  * 	- False when record with desired id is not exist, ovewise True
*/
bool ReqTable_get_revA(byte id, byte *orig, byte *revA) {
	byte rId[9];
	rId[0] = id;
	memcpy(&rId[1], orig, 8);

	struct req *r = Dict_get(req_table, rId, 9);
	if (r == NULL) return false;

	memcpy(revA, r->revA, 8);
	return true;
}

/**
  * @brief  void ReqTable_del(byte id, byte *orig)
  * 		========== Deletes request from Route Table
  *	@param  id: {0-255}
  *		- Route Request ID
  * @param  *orig: byte[8]
  * 	- Originator 64bit address.
  * @retval none
*/
void ReqTable_del(byte id, byte *orig) {
	byte rId[9];
	rId[0] = id;
	memcpy(&rId[1], orig, 8);

	Dict_del_row(req_table, rId, 9);
}

/**
  * @brief  void ReqTable_clean(time_t ctime)
  * 		========== Clean out of date requests from Route Table
  *	@param  ctime: seconds from new era
  *		- Current time
  * @retval none
*/
void ReqTable_clean(time_t ctime) {
	DictIterator iter = Dict_new_iterator(req_table);
	struct req *r = Dict_iterate_values(iter);

	while (r != NULL) {
		if ((ctime - r->time) > ROUTEREQ_LIFETIME)
			ReqTable_del(r->id, r->orig);
		r = Dict_iterate_values(iter);
	}

	Dict_delete_iterator(iter);

}

/**
  * @brief	void ReqTable_print(void)
  *  		========== Prints current Request Table
  * @param  none
  * @retval none
*/
void ReqTable_print(void) {
	DictIterator iter = Dict_new_iterator(req_table);
	struct req *r;
	byte str[16];
	r = Dict_iterate_values(iter);

	puts("\n--------------------- Route Request Table---------------------");
	puts("|  ID  |   Originator   |Rev route address|Fwd RC|Rev RC|Time|");
	while (r != NULL) {
		if (r->time) {
			printf("   %i", r->id);
			hexify(r->orig, str, 8); 	printf("\t%s  ", str);
			hexify(r->revA, str, 8); printf("%s", str);
			printf("   %i ", r->fRC[RC]);
			printf("     %i ", r->rRC[RC]);
			printf("    %i\n", (int) (time(NULL) - r->time)); // If broadcast do not dislpay time - 0
			r = Dict_iterate_values(iter);
		}
	}
}

/**
  * @brief	bool ReqTable_is_in(byte id, byte *orig)
  * 		========== Checks record in Request table.
  *	@param  id: {0-255}
  *		- Route Request ID
  * @param  *orig: byte[8]
  * 	- Originator 64bit address.
  * @retval return: {true, false}
  * 	- true when record is exist in Request Table
  *		- false when record not found in Request Table
*/
bool ReqTable_is_in(byte id, byte *orig) {
	if (Dict_get(req_table, &id, 1) == NULL) return false;
	else return true;
}

//--------------------------------------------------------------------------------

/**
  * @brief	rreq_an(byte *l2pkt, char rssi)
  * 		========== Generates answer for Route Request input LOAD message
  * @param  *l2pkt: byte[any_size], byte[0:1] is array size
  * 	- Input Route Request Message
  * @param  rssi: char
  * 	- RSSI value during recieving this message
  * @retval @param  *l2pkt: byte[any_size], byte[0:1] is array size
  * 	- Output Route Request Message answer (Route Reply)
*/
void rreq_an(byte *l2pkt, char rssi) {
		u8 fRC[2];

		memcpy(&Rreqp_hdr, &l2pkt[0+2], sizeof(Rreqp_hdr));

		if (bytecomp(Rreqp_hdr.orig, eui64, 8)) {
			LOG_TRACE(DEBUG, "orig == my eui");
			byteinit(l2pkt);
			return;
		}

		Rreqp_hdr.wl += (rssi < WEAK_LQI_VALUE);
		fRC[WL] = Rreqp_hdr.wl;
		fRC[RC] = Rreqp_hdr.rc+1;
#ifdef VERBOSE
		byte str[16];
		hexify(p802154.src, str, 8); 	printf("DEBUG: Receive RREQ from %s: ", str);
		hexify(Rreqp_hdr.orig, str, 8); printf("%s->", str);
		hexify(Rreqp_hdr.dst, str, 8); 	printf("%s ", str);
										printf("weak = %i\n", Rreqp_hdr.wl);
#endif
		if (bytecomp(Rreqp_hdr.dst, eui64, 8)) {	// this rreq for me
			LOG_TRACE(DEBUG, "This RREQ for me");
			if (!ReqTable_is_in(Rreqp_hdr.id, Rreqp_hdr.orig) ||
					ReqTable_cmp_fRC(Rreqp_hdr.id, Rreqp_hdr.orig, fRC)) {
#ifdef VERBOSE
				hexify(Rreqp_hdr.dst, str, 8); 	printf("DEBUG: Send RREP to %s via ", str);
				hexify(p802154.src, str, 8);    printf("%s\n", str);
#endif
				ReqTable_new(Rreqp_hdr.id, Rreqp_hdr.orig, p802154.src, fRC);
				RouteTable_new(Rreqp_hdr.orig, p802154.src, VALID);
				Rrep_gen(l2pkt);
			}
		} else { // not for me
			if (ReqTable_is_in(Rreqp_hdr.id, Rreqp_hdr.orig)) {
				if (ReqTable_cmp_rRC(Rreqp_hdr.id, Rreqp_hdr.orig, fRC))
					ReqTable_change(Rreqp_hdr.id, Rreqp_hdr.orig, p802154.src, fRC, NULL);
				byteinit(l2pkt);
				return;
			}
#ifdef VERBOSE
			LOG_TRACE(DEBUG, "This RREQ not for me, forwarding via broadcast");
#endif
			ReqTable_new(Rreqp_hdr.id, Rreqp_hdr.orig, p802154.src, fRC);
			RouteTable_new(Rreqp_hdr.orig, p802154.src, VALID);
			Rreqp_fwd(l2pkt);
		}
		return;
}

/**
  * @brief	rrep_an(byte *l2pkt, char rssi)
  * 		========== Generates answer for Route Reply input LOAD message
  * @param  *l2pkt: byte[any_size], byte[0:1] is array size
  * 	- Input Route Request Message
  * @param  rssi: char
  * 	- RSSI value during recieving this message
  * @retval *l2pkt: byte[any_size], byte[0:1] is array size
  * 	- Output Route Reply Message answer
*/
void rrep_an(byte *l2pkt, char rssi) {
	byte rRC[2];

	Rreqp(l2pkt);
	Rreqp_hdr.wl += (rssi < WEAK_LQI_VALUE);
	rRC[WL] = Rreqp_hdr.wl;
	rRC[RC] = Rreqp_hdr.rc + 1;
#ifdef VERBOSE
	byte str[128];
	hexify(p802154.src, str, 8); 	printf("DEBUG: Receive RREP from %s: ", str);
	hexify(Rreqp_hdr.orig, str, 8); printf("%s->", str);
	hexify(Rreqp_hdr.dst, str, 8); 	printf("%s ", str);
									printf("weak = %i\n", Rreqp_hdr.wl);
#endif
	if(bytecomp(Rreqp_hdr.orig, eui64, 8)) { // this rrep for me
		LOG_TRACE(DEBUG, "rrep for me");
		if (ReqTable_is_in(Rreqp_hdr.id, Rreqp_hdr.orig) &&
				ReqTable_cmp_rRC(Rreqp_hdr.id, Rreqp_hdr.orig, rRC)) {
#ifdef VERBOSE
			hexify(Rreqp_hdr.dst, str, 8); 	printf("DEBUG: This RREP for me. New route to %s via ", str);
			hexify(p802154.src, str, 8);    printf("%s\n", str);
#endif
			RouteTable_new(Rreqp_hdr.dst, p802154.src, VALID);
			ReqTable_upd_rRC(Rreqp_hdr.id, Rreqp_hdr.orig, rRC);
			Limbo_pull(Rreqp_hdr.dst);
			byteinit(l2pkt);
			return;
		}
	} else { // not for me
		if (RouteTable_is_in(Rreqp_hdr.orig) && ReqTable_is_in(Rreqp_hdr.id, Rreqp_hdr.orig) &&
				ReqTable_cmp_rRC(Rreqp_hdr.id, Rreqp_hdr.orig, rRC)) {
			puts("Rrep not for me!!!!");
			ReqTable_upd_rRC(Rreqp_hdr.id, Rreqp_hdr.orig, rRC);
			RouteTable_new(Rreqp_hdr.dst, p802154.src, VALID);
			Rreqp_fwd(l2pkt);
		}
	}
}

/**
  * @brief	void rerr_an(byte *l2pkt, char rssi)
  * 		========== Processing answer for Route Error input LOAD message. If it's not for me do nothing.
  * @param  *l2pkt: byte[any_size], byte[0:1] is array size
  * 	- Input Route Error Message
  * @param  rssi: char
  * 	- RSSI value during recieving this message
  * @retval *l2pkt: byte[any_size], byte[0:1] is array size
  * 	- Output Route Error Message answer if it's not for me. Overwise return nothing.
*/
void rerr_an(byte *l2pkt, char rssi) {
	Rerr(l2pkt);
	if (RouteTable_is_in(Rerr_hdr.unreach)) RouteTable_del(Rerr_hdr.unreach);
	// if Route Error message not for me, return it and retranslate
	if (bytecomp(Rerr_hdr.orig, eui64, 8)) byteinit(l2pkt);
}

/**
  * @brief	load_an(byte *l2pkt, char rssi)
  * 		========== Generates answers on input LOAD messages
  * @param  *l2pkt: Pointer to input data byte array
  * @param  rssi: char
  * 	- RSSI value during recieving this message
  * @retval none
*/
void load_an(byte *l2pkt, char rssi) {
	switch (l2pkt[0+2]) {
	case 1:
		rreq_an(l2pkt, rssi);
		if (!is_data_empty(l2pkt))
			Queue_push(false, l2pkt, Rreqp_hdr.orig, BROADCAST16, ROUTING_PRIORITY);
		break;
	case 2:
		rrep_an(l2pkt, rssi);
		if (!is_data_empty(l2pkt))
			Queue_push(false, l2pkt, Rreqp_hdr.dst, Rreqp_hdr.orig, ROUTING_PRIORITY);
		break;
	case 3:
		rerr_an(l2pkt, rssi);
		if (!is_data_empty(l2pkt))
			Queue_push(false, l2pkt, eui64, Rreqp_hdr.orig, ROUTING_PRIORITY);
		break;
	default:
		printf("ERROR: load_an: %i\n", l2pkt[0+2]);
	}
}

//--------------------------------------------------------------------
// Router routines
//--------------------------------------------------------------------
/**
  * @brief	void Router_init()
  * 		========== Inits LOAD Router. Sets Route Request, Reply and Error form to default values. Inits all tables.
  * @param  none
  * @retval none
*/
void Router_init() {
	memset(&Rreqp_hdr, 0, sizeof(Rreqp_hdr));
	memset(&Rerr_hdr, 0, sizeof(Rreqp_hdr));
	Rerr_hdr.type = 3;
	RouteTable_init();
	ReqTable_init();
	sended_rreq = SeqTable_init();
	sended_rerr = SeqTable_init();
}

/**
  * @brief 	bool Router_has_route(byte *addr)
  * 		========== Checks whether 8 byte destination address in Router Table
  * @param  *dst: byte[8]
  * 	- Destination 64bit address.
  * @retval none
*/
bool Router_has_route(byte *addr) { return RouteTable_is_in(addr); }

/**
  * @brief  void Router_refresh_route(byte *dst, byte *nxt)
  * 		==========Time update of route to destination in route table. If not exist, create new record.
  * @param  *dst: byte[8]
  * 	- Destination 64bit address.
  * @param	*nxt: byte[8]
  * 	- Next hop 64bit address
  * @retval none
*/
void Router_refresh_route(byte *dst, byte *nxt) {
	if (RouteTable_time_up(dst)) return;
	else RouteTable_new(dst, nxt, VALID);
}

/*
* @brief  void Router_error(byte *unreach, byte *orig)
*  		  ========== Creates Route Error message and sends it to originator.
  * @param  *unreach: byte[8]
  * 	- Route unreachable destination 64bit address
   * @param  *orig: byte[8]
  * 	- Route message originator 64bit address
* @retval none
*/
void Router_error(byte *unreach, byte *orig) {
	byte *rerr = malloc(sizeof(Rerr_hdr)+2);

	Rerr_gen(unreach, orig, rerr);
	if (rerr != NULL) {
		Queue_push(false, rerr, eui64, orig, ROUTING_PRIORITY);
		free(rerr);
	}
}

/**
  * @brief  void Router_clean(time_t currtime)
  * 		========== Clean out of date records from all router tables
  *	@param  currtime: seconds from new era
  *		- Current time
  * @retval none
*/
void Router_clean(time_t currtime) {

#ifdef VERBOSE_TABLES
	Router_print();
	ReqTable_print();
#endif

	RouteTable_clean(currtime);
	ReqTable_clean(currtime);
	SeqTable_clean(sended_rreq, currtime);
	SeqTable_clean(sended_rerr, currtime);
}

/**
  * @brief	void Router_make_route(byte *dst)
  *  		========== Creates Route Request message and sends it to BROADCAST.
  * @param  *dst: byte[8]
  * 	- Destination 64bit address.
  * @retval none
*/
void Router_make_route(byte *dst) {
	byte *rreq = malloc(sizeof(Rreqp_hdr)+2);
	u8 fRC[] = {MAX_RC, MAX_RC};

	Rreq_gen(dst, rreq);
	if (rreq != NULL) {
		ReqTable_new(Rreqp_hdr.id, eui64, eui64, fRC);
		Queue_push(false, rreq, eui64, BROADCAST16, ROUTING_PRIORITY);
		free(rreq);
	}
}

//--------------------------------------------------------------------


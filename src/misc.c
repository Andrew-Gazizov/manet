/**
  ******************************************************************************
  * @file    misc.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    23/11/2014
  * @brief   Some misc functions for byte string operations
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <string.h>

/* Library includes. */

/* Headers includes. */
#include "misc.h"

/* Macros --------------------------------------------------------------------*/

/* Variables -----------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/

/**
  * @brief	void byte_swap(byte *data, unsigned short num)
  * 		========== Swaps bytes in string
  * @param  *data: byte[x]
  * 	- Input data
  * @param  num: {2-65535}
  * 	- Num of bytes for swapping
  * @retval none
*/
void byte_swap(byte *data, unsigned short num) {
	byte buf[num];
	unsigned short i;

	memcpy(&buf[0], &data[0], num);
	for (i = 0; i < num; i++) data[num-1-i] = buf[i];
}

/**
  * @brief	void byte_swap_tofrom(byte *data, byte *input, unsigned short num)
  * 		========== Reads byte string and writes swapped bytes to another.
  * @param  *input: byte[x]
  * 	- Input data
  * @param  num: {2-65535}
  * 	- Num of bytes for swapping
  * @retval *input: byte[x]
  * 	- Output data
*/
void byte_swap_tofrom(byte *data, byte *input, unsigned short num) {
	unsigned short i;

	for (i = 0; i < num; i++) data[num-1-i] = input[i];
}

/**
  * @brief	bool startswith(byte *input, byte *comp, unsigned short len)
  * 		========== Checks whether a string begins with the interested
  * @param  *data: byte[x]
  * 	- Input data
  * @param  *comp: byte[x]
  * 	- String for comparing
  * @param  num: {1-65535}
  * 	- Length of string for comparing
  * @retval return: {True, False}
  * 	- True when string one starts with second. Overwise False.
*/
bool startswith(byte *input, byte *comp, unsigned short len) {
	unsigned short i;
	for (i = 0; i < len; i++)
		if (input[i] != comp[i])
			return false;
	return true;
}

/**
  * @brief	bool bytecomp(byte *input, byte *comp, unsigned short len)
  * 		========== Checks for strings match
  * @param  *data: byte[x]
  * 	- Input string
  * @param  *comp: byte[x]
  * 	- String for comparing
  * @param  num: {1-65535}
  * 	- Length of string for comparing
  * @retval return: {True, False}
  * 	- True when strings match. Overwise False.
*/
bool bytecomp(byte *input, byte *comp, unsigned short len) {
	unsigned short i;
	for (i = 0; i < len; i++)
		if (input[i] != comp[i])
			return false;
	return true;
}

/**
  * @brief	void byteinit(byte *data)
  * 		========== Inits byte string in format byte[x+2], byte[0:1] is message size. Sets size to 0.
  * @param  *data: byte[x+2], byte[0:1] is message size
  * 	- Input string
  * @retval  *data: byte[x+2], byte[0:1] is message size
  * 	- Initialized null size string
*/
void byteinit(byte *data) {
	data[0] = 0;
	data[1] = 0;
}

/**
  * @brief	byteadd(byte *data, byte *input)
  * 		========== Adds one string to another in format byte[x+2], byte[0:1] is message size
  * @param  *data: byte[x+2], byte[0:1] is message size
  * 	- Input string to which is added
  * @param  *input: byte[x+2], byte[0:1] is message size
  * 	- String that is added
  * @retval *data: byte[x+2], byte[0:1] is message size
  * 	- Merged strings
*/
void byteadd(byte *data, byte *input) {
	unsigned short index;
	unsigned short num;

	num = (input[1] << 8) | input[0];
	index = (data[1] << 8) | data[0];
	memcpy(&data[index+2], &input[2], num);

	index += num;
	data[0] = (index & 0xFF);
	data[1] = (index >> 8);
}

/**
  * @brief	void byteappend(byte *data, byte *input, unsigned short num)
  * 		========== Adds one string to another
  * @param  *data: byte[x+2], byte[0:1] is message size
  * 	- Input string to which is added
  * @param  *input: byte[x+2], byte[0:1] is message size
  * 	- String that is added
  * @patam  num: {1-65535}
  * 	- Number bytes for appending
  * @retval *data: byte[x+2], byte[0:1] is message size
  * 	- Merged strings
*/
void byteappend(byte *data, byte *input, unsigned short num) {
	unsigned short index;

	index = (data[1] << 8) | data[0];
	memcpy(&data[index+2], &input[0], num);

	index += num;
	data[0] = (index & 0xFF);
	data[1] = (index >> 8);
}

/**
  * @brief	void valueappend(byte *data, byte input)
  * 		========== Adds one byte to string
  * @param  *data: byte[x+2], byte[0:1] is message size
  * 	- Input string to which is added
  * @param  input: {0-0xFF}
  * 	- Byte that is added
  * @retval *data: byte[x+2], byte[0:1] is message size
  * 	- Final string
*/
void valueappend(byte *data, byte input) {
	unsigned short index;

	index = (data[1] << 8) | data[0];
	data[index+2] = input;

	index++;
	data[0] = (index & 0xFF);
	data[1] = (index >> 8);
}

/**
  * @brief	void shortappend(byte *data, unsigned short input)
  * 		========== Adds one short to string
  * @param  *data: byte[x+2], byte[0:1] is message size
  * 	- Input string to which is added
  * @param  input: {0-0xFFFF}
  * 	- Short that is added
  * @retval *data: byte[x+2], byte[0:1] is message size
  * 	- Final string
*/
void shortappend(byte *data, unsigned short input) {
	unsigned short index;

	index = (data[1] << 8) | data[0];
	data[index+2] = input & 0xFF;
	data[index+1+2] = (input >> 8);

	index+=2;
	data[0] = (index & 0xFF);
	data[1] = (index >> 8);
}

/**
  * @brief	void bytecpy(byte *to, byte *from)
  * 		========== Copies byte from one string to another
  * @param  *to: byte[x+2], byte[0:1] is message size
  * 	- Input string from copied
  * @retval *to: byte[x+2], byte[0:1] is message size
  * 	- Output string to which is copied
*/
void bytecpy(byte *to, byte *from) {
	short index;

	index = ((from[1] << 8) | from[0]) + 2;

	for (index; index!=-1; index--) to[index] = from[index];
}

/**
  * @brief	unsigned short bytelen(byte *data)
  * 		========== Gets length of data in byte string in format byte[x+2], byte[0:1] is message size
  * @param  *to: byte[x+2], byte[0:1] is message size
  * 	- Input string
  * @retval return: {0-65533}
  * 	- Size of data in input string
*/
unsigned short bytelen(byte *data) {
	return ((data[1] << 8) | data[0]);
}

/**
  * @brief	bool is_data_empty(byte *input)
  * 		========== Checks whether a string is empty
  * @param  *input: byte[x+2], byte[0:1] is message size
  * @retval return: {True, False}
  * 	- True when string is empty. Overwise False.
*/
bool is_data_empty(byte *input) {
	return ((input[0] == 0) && (input[1] == 0));
}




/**
  ******************************************************************************
  * @file    sixlowpan_defragmenter.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    26/11/2014
  * @brief   This file contains global variables and function prototypes for
  * 		 6LowPAN packet managment
  ******************************************************************************
**/

#ifndef SIXLOWPAN_H_
#define SIXLOWPAN_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */

/* Library includes. */

/* Headers includes. */
#include "misc.h"

/* Macros --------------------------------------------------------------------*/

/* Function Prototypes --------------------------------------------------------*/
void Lowpan_serialize_for_retranslation(byte *);
bool is_correct_not_lowpan(byte *);
void Lowpan_init(byte *, byte *, byte *);
void LowpanGenerator(byte *, unsigned short, bool, bool);
bool Lowpan_get_whole_payload(byte *);
bool is_compressed(byte *);
void sixlowpan_clean(time_t);
void sixlowpan_init(void);

// LoWPAN packet form
struct {
	u8 hops_left;
	byte src[8];
	byte dst[8];
	u8 mesh_len;
	u8 broadcast_seq;
	byte raw[256];
} lowpan;

#endif /* SIXLOWPAN_H_ */

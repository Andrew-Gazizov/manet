/**
  ******************************************************************************
  * @file    chip_master.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    22/11/2014
  * @brief   Provides chip control and serial data I/O
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <time.h>
#include <stdlib.h>

/* Library includes. */

/* Headers includes. */
#include "chip_master.h"
#include "manet.h"
#include "binascii.h"
#include "logger.h"
#include "sslip.h"
#include "node.h"
#include "dbus.h"

/* Macros --------------------------------------------------------------------*/
#define UART_FRAME_SIZE RADIO_FRAME_SIZE * 2 + 2
#define ACK_TIMEOUT MAX_ACK_TIMEOUT // in sec

/* Variables -----------------------------------------------------------------*/
static time_t send_time;
byte cm_expected_answer;

/* Function Prototypes -------------------------------------------------------*/
void commands(byte, byte *);
void read_cmd_echo(byte *);
void cm_raw_write(byte, byte *);
void turn_off_radio(void);
bool cm_write_and_read(byte *, unsigned short);
int (*check_cts)(int);

static u8 state, payload_len;

/**
  * @brief	void serial_reader()
  * 		========== Reads serial data from chip and transfer to it's processing
  * @param  none
  * @retval none
*/
void serial_reader(void) {
	static byte cmd;
	static unsigned short length;
	static byte data[512];

	u8 n = UART_FRAME_SIZE;
	unsigned short i, rest_len, need_len;

	byte *input = malloc(UART_FRAME_SIZE+2);
	malloc_count++;

	n = RS232_PollComport(serial_fd, &input[2], UART_FRAME_SIZE);
	input[0] = n & 0xFF;
	input[1] = (n >> 8) & 0xFF;
	LOG_TRACE(DEBUG, "SSLIP numbytes %i", n);

	i = 2;
	while (i < (input[0]+2)) {
		switch (state) {
		case 0: if (input[i] == 0x63) state = 1; i++; break;
		case 1: if (input[i] == 0x6D) state = 2; i++; break;
		case 2: if (input[i] == 0x64) state = 3; i++; break;
		case 3: cmd = input[i];  state = 4; i++; break;
		case 4:
			length = input[i]; i++;
			if (length) {
				state = 5;
				byteinit(data);
				payload_len = 0;
			} else {
				byteinit(data);
				data[2] = cmd;
				state = 0;
			}
		case 5:
			rest_len = n -(i-2);
			need_len = length - payload_len;
			if (need_len <= rest_len) {
				byteappend(data, &input[i], need_len);
				i += need_len;
				commands(cmd, data);
				if (cmd == cm_expected_answer)  cm_expected_answer = None;
				state = 0;
				break;
			} else {
				byteappend(data, &input[i], rest_len);
				payload_len += rest_len;
				i += rest_len;
				goto out;
			}
		default: state = 0; goto out;
		}
	}
out:
	free(input);
	malloc_count--;
}

/**
  * @brief	static inline int check_cts_cc430(int serial_fd);
  * 		========== For CC430 CTS signal check is no needed, so this is dummy inline function
  * @param  none
  * @retval return: 1
  * 	- Operation result.
*/
static inline int check_cts_cc430(int serial_fd) { return 1; }

/**
  * @brief	unsigned short ChipMaster_Init()
  * 		========== Opens COM-port and inits chip
  * @param  none
  * @retval return: {0,2}
  * 	- Operation result.
*/
unsigned short ChipMaster_Init() {
	char mode[]={'8','N','1', 0};

	if(RS232_OpenComport(serial_fd, cfg.port, cfg.baud, mode))  {
		LOG_TRACE(ERROR, "Cannot open comport\n");
	    return 2;
	}

	if (!strcmp(cfg.chip, "cc430")) check_cts = &check_cts_cc430;
	else check_cts = &RS232_IsCTSEnabled;

	state = 0;
	RS232_enableRTS(serial_fd);

	while (check_cts == 0);

	cm_expected_answer = None;

	turn_off_radio();
	get_eui64();
	return 0;
}

/**
  * @brief	void restore_settings()
  * 		========== Reset chip settings to it's default
  * @param  none
  * @retval none
*/
void restore_settings() {
	byte msg[4] = {0, 1, 0 ,0};

	turn_off_radio();

	msg[0] = CMD_ANTENNA;
	if (!strcmp(cfg.antenna, "internal"))
		msg[2] = 1;
	else if (!strcmp(cfg.antenna, "external"))
		msg[2] = 1;
	if (!cm_write_and_read(msg, 1))
		goto err;

	msg[0] = CMD_SNIFF_MODE;
	msg[2] = 0;
	if (!cm_write_and_read(msg, 1))
		goto err;

	msg[0] = CMD_RADIO_CHANNEL;
	msg[2] = cfg.channel;
	if (!cm_write_and_read(msg, 1))
		goto err;

	msg[0] = CMD_TX_POWER;
	msg[2] = cfg.power;
	if (!cm_write_and_read(msg, 1))
		goto err;

	turn_on_radio();
	return;

err:
	LOG_TRACE(ERROR, "Error during restoring settings");
	exit(1);
}

/**
  * @brief	complete_init()
  * 		========== Completes chip initialization
  * @param  none
  * @retval none
*/
void complete_init() {
	restore_settings();
}

/**
  * @brief	bool is_idle()
  * 		========== Checks whether chip is idle
  * @param  none
  * @retval return: {True, False}
  * 	- True when chip is idle. Overwise False.
*/
bool is_idle(void) {
	return (cm_expected_answer == None);
}

/**
  * @brief	void cm_raw_write(byte cmd, byte *msg)
  * 		========== Sends command and message to chip
  * @param  cmd: {0-0xFF}
  * 	- Command to send
  * @param  *msg: byte[x+2], byte[0:1] is message size
  * 	- Message to send
  * @retval none
*/
void cm_raw_write(byte cmd, byte *msg) {
	byte str[3] =  "cmd";

	while (check_cts == 0);
	RS232_SendBuf(serial_fd, str, 3);
	RS232_SendByte(serial_fd, cmd);
	RS232_SendBuf(serial_fd, &msg[0], msg[0]+1);

	send_time = time(NULL);
	cm_expected_answer = cmd;
	return;
}

/**
  * @brief	void chip_write(byte *msg)
  * 		========== Sends message to chip and radio
  * @param  *msg: byte[x+2], byte[0:1] is message size
  * 	- Message to send
  * @retval none
*/
void chip_write(byte *msg) {
	LOG_TRACE(DEBUG, "Writing to chip");
	byte temp = msg[1];							// FIXME: Debugging dummy thing
	msg[1] = msg[0];
	cm_raw_write(CMD_SEND, &msg[1]);
	msg[1] = temp;
	return;
}

/**
  * @brief	void read_cmd_error(byte *msg)
  * 		========== Processing chip error
  * @param  *msg: byte[x]
  * 	- Input message
  * @retval none
*/
void read_cmd_error(byte *msg) {
	switch (msg[1]) {
	case 0:
		LOG_TRACE(ERROR, "CHIP: unknown cmd, err_code: %x\n", msg[0]);
		return;
	case 1:
		LOG_TRACE(ERROR, "CHIP: wrong length, err_code: %x\n", msg[0]);
		return;
	case 2:
		LOG_TRACE(ERROR, "CHIP: wrong format, err_code: %x\n", msg[0]);
		return;
	case 3:
		LOG_TRACE(ERROR, "CHIP: wrong conditions, err_code: %x\n", msg[0]);
		return;
	default:
		LOG_TRACE(ERROR, "CHIP: Unknown error");
	}

	if (msg[0] == cm_expected_answer) cm_expected_answer = None;
}

/**
  * @brief	void read_cmd_send(byte *msg)
  * 		========== Processing chip response for sending data
  * @param  *msg: byte[x]
  * 	- Input message
  * @retval none
*/
void read_cmd_send(byte *msg) {
	if (msg == BAD_RESPONCE) process_chip_bad_responce();
}

/**
  * @brief	void read_cmd_echo(byte *msg)
  * 		========== Processing chip response for echo
  * @param  *msg: byte[x]
  * 	- Input message
  * @retval none
*/
void read_cmd_echo(byte *msg) {
	//logInfo(BinToHexString(msg, 12));
}

/**
  * @brief	void read_cmd_echo(byte *msg)
  * 		========== Processing chip response for echo
  * @param  *msg: byte[x]
  * 	- Input message
  * @retval none
*/
void read_cmd_radio_mode(byte *msg) {
	switch (msg[0]) {
	case 1:
		LOG_TRACE(INFO, "Current radio mode is OFF");
//		if (cfg.dbus)
//			dbus_method_reply("off");
		break;
	case 3:
		LOG_TRACE(INFO, "Current radio mode is ON");
//		if (cfg.dbus)
//			dbus_method_reply("on");
		break;
	default: LOG_TRACE(ERROR, "Invalid radio mode %i\n", msg[0]);
	}

}

/**
  * @brief	void read_cmd_get_ver_eui(byte *msg)
  * 		========== Processing chip response for get chip info
  * @param  *msg: byte[x]
  * 	- Input message
  * @retval none
*/
void read_cmd_get_ver_eui(byte *msg) {
	byte ver[11];
	byte t[4];

	memcpy(&ver[0], &msg[0], 11);
	memcpy(&eui64[0], &msg[11], 8);
	byte_swap(eui64, 8);
	memcpy(&t[0], &msg[19], 4);

	CONN_STATUS = 1;
}

/**
  * @brief	void read_cmd_restart()
  * 		========== Processing chip response for restart
  * @param  none
  * @retval none
*/
void read_cmd_restart(void) {
	LOG_TRACE(INFO, "Chip restart");
	restore_settings();
}

/**
  * @brief	bool cm_write_and_read(byte *msg, unsigned short timeout)
  * 		========== Sends some message to chip and wait for chip reply
  * @param  *msg: byte[x]
  * 	- Input message
  * @param timeout: in secs
  * 	- Chip responce timeout
  * @retval return: {True, False}
  * 	- True when payload sent and received expected answer (operation without errors)
*/
bool cm_write_and_read(byte *msg, unsigned short timeout) {
    u8 len_s, i, length;
    byte buf[256];
    byte data[256];
    time_t stop_time;
    byte cmd;

	// send some msg to chip and wait for chip reply
	if (timeout == 0) timeout = ACK_TIMEOUT;

	stop_time = time(NULL) + timeout;

	cmd = msg[0];
 	cm_raw_write(cmd, &msg[1]);
 	byteinit(data);
	while (time(NULL) < stop_time) {
		len_s = RS232_PollComport(serial_fd, buf, UART_FRAME_SIZE);
		if (len_s < 5) continue;
		i = sslip_find_data_pos(buf, len_s, cmd);
		if (i == 0xFF) continue;

		length = buf[i]; i++;
		byteinit(data);
		if (length) byteappend(data, &buf[i], length);
		commands(cmd, &data[2]);
		if (cmd == cm_expected_answer) cm_expected_answer = None;
		return true;
	}
	return false;
 }

/**
  * @brief	bool is_timeout()
  * 		========== Checks whether the timeout occurred
  * @param  none
  * @retval return: {True, False}
  * 	- True when timeout is occured. Overwise False.
*/
bool is_timeout() {
	/* check chip answer timeout
	   NB! use only if chip isn't idle */
	if ((time(NULL) - send_time) > ACK_TIMEOUT) {
		LOG_TRACE(ERROR, "CHIP DIDN'T REPLY");
		cm_expected_answer = None;
		send_time = time(NULL);
		return true;
	}
	return false;
}

/**
  * @brief	void get_eui64()
  * 		========== Send chip command for get EUI64 and processing responce
  * @param  none
  * @retval none
*/
void get_eui64() {
	byte msg[2];

	while (true) {
		msg[0] = CMD_GET_VER_EUI;
		msg[1] = 0;
		if (cm_write_and_read(msg, 1)) return;		// If eui64 is got, then return try again.
	}
}

/**
  * @brief	void turn_off_radio()
  * 		========== Turns off chip radio
  * @param  none
  * @retval none
*/
void turn_off_radio() {
	byte msg[3];

	while (true) {
		msg[0] = CMD_RADIO_MODE;
		msg[1] = 1;
		msg[2] = 1;
		if (cm_write_and_read(msg, 1)) return;		// If eui64 is got, then return try again.
	}
}

/**
  * @brief	void turn_off_radio()
  * 		========== Turns on chip radio
  * @param  none
  * @retval none
*/
void turn_on_radio() {
	byte msg[3];

	while (true) {
		msg[0] = CMD_RADIO_MODE;
		msg[1] = 1;
		msg[2] = 3;
		if (cm_write_and_read(msg, 1)) return;		// If eui64 is got, then return try again.
	}
}

/**
  * @brief	void get_radio_mode()
  * 		========== Send command for retrieving radio mode info
  * @param  none
  * @retval none
*/
void get_radio_mode() {
	byte msg[2];

	while (true) {
		msg[0] = CMD_RADIO_MODE;
		msg[1] = 0;
		if (cm_write_and_read(msg, 1)) return;		// If eui64 is got, then return try again.
	}
}

/**
  * @brief	commands(byte cmd, byte *payload)
  * 		========== Processes chip responce by call appropriate handler
  * @param  cmd: {0-0xFF}
  * 	- Sent command
  * @param  *payload: byte[x+2], byte[0:1] is message size
  * 	- Received chip response
  * @retval none
*/
void commands(byte cmd, byte *payload) {
	switch (cmd) {
	case CMD_ERROR: 		read_cmd_error(payload); 				break;
	case CMD_SEND: 			read_cmd_send(payload);  				break;
	case CMD_RECV:  		process_data_from_outside(payload); 	break;
	case CMD_ECHO:  		read_cmd_echo(payload); 				break;
	case CMD_GET_VER_EUI: 	read_cmd_get_ver_eui(payload); 			break;
	case CMD_RADIO_CHANNEL: break;
	case CMD_RADIO_MODE:	read_cmd_radio_mode(payload); 			break;
	case CMD_TX_POWER:		break;
	case CMD_ANTENNA: 		break;
	case CMD_SNIFF_MODE: 	break;
	default: 				LOG_TRACE(INFO, "Unknown chip command %x\n", cmd);
	}
}

/**
  ******************************************************************************
  * @file    sixlowpan_compressor.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    26/11/2014
  * @brief   6LowPAN compressor & decompressor routines
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <stdio.h>
#include <string.h>

/* Library includes. */

/* Headers includes. */
#include "sixlowpan_compressor.h"
#include "misc.h"
#include "iptools.h"

/* Macros --------------------------------------------------------------------*/
#define SENT_IN_LINE 	0
#define UDP 			1
#define ICMP 			2
#define TCP 			3

#define SHORT_PORTS_PREFIX 	0xF0B0

/* Variables -----------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/

//-----------------------------------------------------------------
// HC1 Routines
//-----------------------------------------------------------------

// HC1 header form
struct {
	bool src_is_local;
	bool src_iid_is_elided;
	bool dst_is_local;
	bool dst_iid_is_elided;
	bool tr_cl_and_fl_lb_are_zero;
	unsigned char nxt_type:2;
	bool HC2_is_followed;
} hc1;

/**
  * @brief	void hc1_init()
  * 		========== Inits hc1 struct with default falues
  * @param  none
  * @retval none
*/
void hc1_init() {
	hc1.src_is_local = false;
	hc1.src_iid_is_elided = false;
	hc1.dst_is_local = false;
	hc1.dst_iid_is_elided = false;
	hc1.tr_cl_and_fl_lb_are_zero = false;
	hc1.nxt_type = SENT_IN_LINE;
	hc1.HC2_is_followed = false;
}

/**
  * @brief	void hc1_deserialize(byte data)
  * 		========== Stores HC1 header to hc1 struct
  * @param  data: {0-0xFF}
  * 	- HC1 header
  * @retval none
*/
void hc1_deserialize(byte data) {
	hc1.src_is_local = (data >> 7) & 0b1;
	hc1.src_iid_is_elided = (data >> 6) & 0b1;
	hc1.dst_is_local = (data >> 5) & 0b1;
	hc1.dst_iid_is_elided = (data >> 4) & 0b1;
	hc1.tr_cl_and_fl_lb_are_zero = (data >> 3) & 0b1;
	hc1.nxt_type = (data >> 1) & 0b11;
	hc1.HC2_is_followed = data & 0b1;
}

/**
  * @brief	unsigned char hc1_serialize()
  * 		========== Gets HC1 header from hc1 struct
  * @param 	none
  * @retval return: {0-0xFF}
  * 	- HC1 header
*/
unsigned char hc1_serialize() {
	return (hc1.src_is_local << 7 | (hc1.src_iid_is_elided << 6) | (hc1.dst_is_local << 5) |
			(hc1.dst_iid_is_elided << 4) |	(hc1.tr_cl_and_fl_lb_are_zero << 3) |
			(hc1.nxt_type << 1) | hc1.HC2_is_followed);
}

//-----------------------------------------------------------------
// HC2 Routines
//-----------------------------------------------------------------

// HC2 header form
struct hc2_struct {
	bool src_port_is_short;
	bool dst_port_is_short;
	bool udp_len_is_elided;
} hc2;

/**
  * @brief	void hc2_init()
  * 		========== Inits hc2 struct with default falues
  * @param  none
  * @retval none
*/
void hc2_init() {
	hc2.src_port_is_short = false;
	hc2.dst_port_is_short = false;
	hc2.udp_len_is_elided = false;
}

/**
  * @brief	void hc2_deserialize(byte data)
  * 		========== Stores HC2 header to hc2 struct
  * @param  data: {0-0xFF}
  * 	- HC2 header
  * @retval none
*/
void hc2_deserialize(byte data) {
	hc2.src_port_is_short = data >> 7;
	hc2.dst_port_is_short = (data >> 6) & 0b1;
	hc2.udp_len_is_elided = (data >> 5) & 0b1;
}

/**
  * @brief	void hc2_init_data()
  * 		========== Inits hc2 struct with specific data
  * @param  byte: {0-0xFF}
  * 	- HC2 header
  * @retval none
*/
void hc2_init_data(byte hc2_header) {
	hc2_deserialize(hc2_header);
}

/**
  * @brief	unsigned char hc2_serialize()
  * 		========== Gets HC2 header from hc2 struct
  * @param 	none
  * @retval return: {0-0xFF}
  * 	- HC2 header
*/
unsigned char hc2_serialize() {
	return ((hc2.src_port_is_short << 7) | (hc2.dst_port_is_short << 6) | (hc2.udp_len_is_elided << 5));
}
//-----------------------------------------------------------------

/**
  * @brief	void compress(byte *ip, bool is_marked, byte *r)
  * 		========== Compresses input IPv6 packet
  * @param  *msg: byte[x+2], byte[0:1] is message size
  * 	- Input IPv6 packet
  * @param is_marked: {true, false}
  * 	- true when packet is marked. Overwise false.
  * @retval  *r: byte[x+2], byte[0:1] is message size
  * 	- Output compressed packet
*/
void compress(byte *ip, bool is_marked, byte *r) {
	byte ports;

	byteinit(r);
	if (is_marked) valueappend(r, 0x43);
	else valueappend(r, 0x42);

	hc1_init();
	hc1.src_is_local = startswith(&ip6.src[0], LOCAL_PREFIX, 8);
	hc1.src_iid_is_elided = true;
	hc1.dst_is_local = startswith(&ip6.dst[0], LOCAL_PREFIX, 8);
    hc1.dst_iid_is_elided = (!is_broadcast(&ip6.dst[0]));

    hc1.tr_cl_and_fl_lb_are_zero = ((ip6_tc() == 0) & (ip6_flow() == 0));

    switch (ip6.nxt) {
    case 17:
    	hc1.nxt_type = UDP;
    	break;
    case 58:
        hc1.nxt_type = ICMP;
        break;
    case 6:
        hc1.nxt_type = TCP;
        break;
    default:
    	hc1.nxt_type = SENT_IN_LINE;
    }

    hc1.HC2_is_followed = ip6_is_udp();
    valueappend(r, hc1_serialize());

    if (hc1.HC2_is_followed) {
        hc2_init();
        UDP_init(ip6.data);
        hc2.src_port_is_short = ((udp.sport >> 4) == (SHORT_PORTS_PREFIX >> 4));
        hc2.dst_port_is_short = ((udp.dport >> 4) == (SHORT_PORTS_PREFIX >> 4));
        hc2.udp_len_is_elided = true;
        valueappend(r, hc2_serialize());
    }

   valueappend(r, ip6.hlim);

   if (!hc1.src_is_local)
    	byteappend(r, &ip6.src[0], 8);
    if (!hc1.src_iid_is_elided)
    	byteappend(r, &ip6.src[8], 8);
    if (!hc1.dst_is_local)
    	byteappend(r, &ip6.dst[0], 8);
    if (!hc1.dst_iid_is_elided)
    	byteappend(r, &ip6.dst[8], 8);
    if (!hc1.tr_cl_and_fl_lb_are_zero) {
    	valueappend(r, ip6_tc());
    	valueappend(r, ((ip6_flow() & 0xFF0000) >> 16));

    	valueappend(r, (ip6_flow() & 0xFF));
    	valueappend(r, ((ip6_flow() >> 2) & 0xFF));
    }

    if (hc1.nxt_type == SENT_IN_LINE)
    	valueappend(r, (ip6_flow() & 0x00FFFF));

    if (hc1.HC2_is_followed) {
        if (hc2.src_port_is_short & hc2.dst_port_is_short) {
        	ports = (((udp.sport & 0xF) << 4) | (udp.dport & 0xF));
        	valueappend(r, ports);
        } else
        	if (!hc2.src_port_is_short) {
        		valueappend(r, (udp.sport & 0xF));
        		valueappend(r, udp.dport);
        	} else
        		if (!hc2.dst_port_is_short) {
            		valueappend(r, udp.sport);
            		valueappend(r, (udp.dport & 0xF));
        		} else {
            		shortappend(r, udp.sport);
            		shortappend(r, udp.dport);
        		}
        if (!hc2.udp_len_is_elided) {
        	shortappend(r, udp.ulen);
        }
        byteappend(r, (byte *) &udp.sum, 2);
        byte_swap((byte *) &udp.ulen, 2);
        byteappend(r, (byte *) &udp.data, udp.ulen - 8);
    } else {
    	byteappend(r, ip6.data, ip6.plen);
    }
}

/**
  * @brief	bool decompress(byte *compressed, u8 hlim, byte *src, byte *dst)
  * 		========== Decompresses input packet into ip6 struct
  * @param  *compressed: byte[x+2], byte[0:1] is message size
  * 	- Input binary LowPAN packet without mesh and broadcast headers
  * @param hlim: {0-255}
  * 	- Packet hop limit
  * @param *src: byte[8]
  * 	- 64bit L2 source address
  * @param *dst: byte[8]
  * 	- 64bit L2 destination address
  * @retval return: {true, false}
  * 	- true when decompressing was succeccful, overwise false
*/
bool decompress(byte *compressed, u8 hlim, byte *src, byte *dst) {
	byte db;
	byte src_prefix[8];
	byte dst_prefix[8];
	u8 iter = 0;

	db = compressed[2+iter]; iter++;
	if ((db != 0x42) & (db != 0x43)) {
		printf("ERROR: wrong dispatch byte in compressed lowpan: %x \n", db);
		return false;	// returns error when byte dispatch is missing
	}

	hc1_deserialize(compressed[2+iter]); iter++;

	if (hc1.HC2_is_followed) {
		hc2_init_data(compressed[2+iter]); iter++;
	}
	ip6_init(NULL);
	ip6.hlim = MAX(compressed[2+iter], hlim);
	iter++;

	if (hc1.src_is_local)
		memcpy(src_prefix, LOCAL_PREFIX, 8);
	else {
		memcpy(src_prefix, &compressed[2+iter], 8);
		iter += 8;
	}
	if (hc1.src_iid_is_elided) l2_to_ip(src, src_prefix, ip6.src);
	else {
		memcpy(ip6.src, src_prefix, 8);
		memcpy(&ip6.src[8], &compressed[2+iter], 8);
		iter += 8;
	}
	if (hc1.dst_is_local) memcpy(dst_prefix, LOCAL_PREFIX, 8);
	else {
		memcpy(dst_prefix, &compressed[2+iter], 8);
		iter += 8;
	}
	if (hc1.dst_iid_is_elided) l2_to_ip(dst, dst_prefix, ip6.dst);
	else {
		memcpy(ip6.dst, dst_prefix, 8);
		memcpy(&ip6.dst[8], &compressed[2+iter], 8);
		iter += 8;
	}
	if (hc1.tr_cl_and_fl_lb_are_zero) {
		ip6_tc_set(0);
		ip6_flow_set(0);
	} else {
		ip6_tc_set(compressed[2+iter]);
		iter++;
		int flow_set;
		memcpy(&flow_set, &compressed[2+iter], 4);
		ip6_flow_set(flow_set);
		iter += 3;
	}

	switch (hc1.nxt_type & 0b11) {
	case UDP:
		ip6.nxt = 17;
		break;
	case ICMP:
		ip6.nxt = 58;
		break;
	case TCP:
		ip6.nxt = 6;
		break;
	default:
		puts("INFO: Unknown next header, get from pkt.");
		hc1.nxt_type = compressed[2+iter];
		iter++;
	}

	if (hc1.HC2_is_followed) {
		if (ip6.nxt != 17) puts("ERROR: LoWPAN: incorrect not udp packet with hc2 byte");
		UDP_init(NULL);
		if (hc2.src_port_is_short & hc2.dst_port_is_short) {
			udp.sport = SHORT_PORTS_PREFIX | (compressed[2+iter] >> 4);
			udp.dport = SHORT_PORTS_PREFIX | (compressed[2+iter] & 0xF);
			iter++;
		} else if (hc2.src_port_is_short) {
			udp.sport = SHORT_PORTS_PREFIX | compressed[2+iter];
			iter++;
			udp.dport = (compressed[2+iter] << 8) | compressed[2+iter+1];
			iter += 2;
		} else if (hc2.dst_port_is_short) {
			udp.sport = (compressed[2+iter] << 8) | compressed[2+iter+1];
			iter += 2;
			udp.dport = SHORT_PORTS_PREFIX | compressed[2+iter];
			iter++;
		} else {
			udp.sport = (compressed[2+iter] << 8) | compressed[2+iter+1];
			iter += 2;
			udp.dport = (compressed[2+iter] << 8) | compressed[2+iter+1];
			iter += 2;
		}
		if (!hc2.udp_len_is_elided) {
			udp.ulen = (compressed[2+iter+1] << 8) | compressed[2+iter];
			iter += 2;
		}
		udp.sum = (compressed[2+iter+1] << 8) | compressed[2+iter];
		iter += 2;
		memcpy(udp.data, &compressed[iter+2], ((compressed[1] << 8) | compressed[0]) - iter);
		if (hc2.udp_len_is_elided) {
			udp.ulen = ((compressed[1] << 8) | compressed[0]) - iter + 8;
		}
		byteinit(ip6.data);
		byte_swap((byte *) &udp.sport, 2);
		byte_swap((byte *) &udp.dport, 2);
		byte_swap((byte *) &udp.ulen, 2);
		memcpy(ip6.data, &udp, ((compressed[1] << 8) | compressed[0]) - iter + 8);
		ip6.plen = ((compressed[1] << 8) | compressed[0]) - iter + 8;
	} else {
		ip6.plen = ((compressed[1] << 8) | compressed[0]) - iter;
		byteinit(ip6.data);
		memcpy(ip6.data, &compressed[iter+2], ip6.plen);
	}
	return true;
}



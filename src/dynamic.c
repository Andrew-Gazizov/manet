/**
  ******************************************************************************
  * @file    dynamic.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    17/08/2015
  * @brief   Provides dynamic structures: dictionaties and lists
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <stdlib.h>
#include <string.h>

/* Library includes. */

/* Headers includes. */
#include "dynamic.h"
#include "binascii.h"

/* Macros --------------------------------------------------------------------*/
//#define DEBUG

/* Function Prototypes --------------------------------------------------------*/

//------------------------------------------------------------------
//		Dictionaries
//------------------------------------------------------------------

// Struct represents row of table. Contains two coloumns with key and value
// and pointers to it's neighbours
struct DictRowStruct {
	DictRow prev;
	void *key;
	void *value;
	DictRow next;
};

// Struct represents one dictionary
struct DictHeader {
	DictRow first;
	DictRow last;
	unsigned short total_rows;
};

// Struct represents iteration for dictionary
struct DictIteratorStruct {
	Dict dict;
	DictRow current;
};

/**
  * @brief  Dict Dict_new(void)
  * 		========== Creates new dictionary
  * @retval return: struct Dict
  * 	- New dictionary struct
*/
Dict Dict_new(void) {
	Dict new_dict = malloc(sizeof(struct DictHeader));

	// Init free dict
	new_dict->total_rows = 0;
	new_dict->first = new_dict->last = NULL;

	return new_dict;
}

/**
* @brief  void Dict_new_row(Dict dict, void *key, unsigned short key_size,
* 			void *value, unsigned short value_size)
* 		========== Creates new entry in table (row)
* @param  dict: struct Dict
* 	- Structure of the dictionary where you want to add a row
* @param  *key: void[x]
* 	- Pointer to key field for new row
* @param  *key_size: {1-65535}
* 	- Size of passing key
* @param  *value: void[x]
* 	- Pointer to value field for new row
* @param  *value_size: {1-65535}
* 	- Size of passing value
* @retval none
*/
void Dict_new_row(Dict dict, void *key, unsigned short key_size, void *value,
		unsigned short value_size) {
	DictRow new_row = malloc(sizeof(struct DictRowStruct));

	new_row->key = malloc(key_size);
	new_row->value = malloc(value_size);
	new_row->next = NULL; 	// if row we adding to end of dict

	memcpy(new_row->key, key, key_size);
	memcpy(new_row->value, value, value_size);

	if (dict->total_rows == 0) {
		dict->first = dict->last = new_row;
		new_row->prev = NULL;
	} else {
		new_row->prev = dict->last;
		dict->last->next = new_row;
		dict->last = new_row;
	}

	dict->total_rows++;
}

/**
* @brief  DictIterator Dict_new_iterator(Dict dict)
* 		========== Creates new iterator for retrieving rows from dict
* @param  dict: struct DictHeader
* 	- Dictionary for iteration
* @retval return: struct DictIteraror
* 	- New dictionary iterator
*/
DictIterator Dict_new_iterator(Dict dict) {
	DictIterator iter = malloc(sizeof(struct DictIteratorStruct));
	iter->dict = dict;
	iter->current = dict->first;
	return iter;
}

/**
* @brief  void Dict_delete_iterator(DictIterator iter)
* 		========== Deletes desired iterator
* @param  iter: struct DictIterator
* 	- Iterator for deleting
* @retval none
*/
void Dict_delete_iterator(DictIterator iter) { free(iter); }

/**
* @brief  DictRow Dict_iterate(DictIterator iter)
* 		========== Iterates desired iterator and returns rows from dict
* @param  iter: struct DictIterator
* 	- Iterator for iteration
* @retval return: struct DictRow
* 	- Returns next row from dictionary. Returns NULL when iteration was
* 		finished and when dictionary is empty. 	When you try to iterate it's
* 		again iteration spend from beginning.
*/
DictRow Dict_iterate(DictIterator iter) {
	if (iter->current == NULL) {
		//reset iterator to first position and return NULL pointer
		iter->current = iter->dict->first;
		return NULL;
	}

	DictRow row = iter->current;
	iter->current = iter->current->next;

	return row;
}

/**
* @brief  DictRow Dict_iterate(DictIterator iter)
* 		========== Iterates desired iterator and returns values of rows from dict
* @param  iter: struct DictIterator
* 	- Iterator for iteration
* @retval return: void *
* 	- Returns next row value from dictionary. Returns NULL when iteration was finished and
* 		when dictionary is empty. When you try to iterate it's again iteration spend from beginning.
*/
void *Dict_iterate_values(DictIterator iter) {
	DictRow row = Dict_iterate(iter);
	if (row == NULL) return NULL;
	else return row->value;
}

/**
* @brief  void *Dict_get(Dict dict, void *key, unsigned short size
* 		========== Gets row with requested key from dictionary
* @param  dict: struct DictHeader
* 	- Dictionary from we must get row
* @param  *key: void[x]
* 	- Pointer to key field for find
* @param  *key_size: {1-65535}
* 	- Size of passing key
* @retval return: struct DictRow
* 	- Returns pointer with data from dictionary with desired key. Returns NULL when row with requested
* 		field was not found and when dictionary is empty.
*/
void *Dict_get(Dict dict, void *key, unsigned short size) {
	DictIterator iter = Dict_new_iterator(dict);

	DictRow row;
	row = Dict_iterate(iter);

	while (row != NULL) {
		if (bytecomp(row->key, key, size)) break;
		row = Dict_iterate(iter);
	}

	Dict_delete_iterator(iter);
	if (row != NULL) return row->value;
	else return NULL;
}

#ifdef DEBUG
void Dict_print(Dict dict) {
	DictIterator iter = Dict_new_iterator(dict);
	DictRow row;
	u8 pos = 0;

	row = Dict_iterate(iter);

	puts("\n--------------- Dict Table --------------");
	puts("|POS|	KEY	|	VALUE		|");

	while (row != NULL) {
		printf("  %i", pos);
		printf("\t%s  ", row->key);
		printf("\t\t%s\n", row->value);
		pos++;
		row = Dict_iterate(iter);
	}
	Dict_delete_iterator(iter);
}

#endif

/**
* @brief  u8 Dict_del_row(Dict dict, void *key, unsigned short size)
* 		========== Deletes row from dict with specified key
* @param  dict: struct DictHeader
* 	- Dictionary thats row must be removed
* @param  *key: void[x]
* 	- Key of row thats mus be removed
* @param  *size: {1-65535}
* 	- Size of key for matching
* @retval return: {0, 1}
* 	- Returns True when row was removed successfully.
* 		Returns False when row with passed key is not exist.
*/
bool Dict_del_row(Dict dict, void *key, unsigned short size) {
	DictIterator iter = Dict_new_iterator(dict);
	DictRow row;

	row = Dict_iterate(iter);

	while (row != NULL) {
		if (bytecomp(key, row->key, size)) {
			free(row->key);
			free(row->value);
			//when row is single
			if (dict->total_rows == 1) {
				dict->first = dict->last = NULL;
				goto out;
			} else
			//when row is first
			if (row->prev == NULL) {
				dict->first = row->next;
				dict->first->prev = NULL;
				goto out;
			} else
			//when row is last
			if (row->next == NULL) {
				dict->last = row->prev;
				dict->last->next = NULL;
				goto out;
			}
			// when row in middle of table
			row->prev->next = row->next;
			row->next->prev = row->prev;
			goto out;
		}
		row = Dict_iterate(iter);
	}

		Dict_delete_iterator(iter);
		return false;
out:
		free(row);
		dict->total_rows--;
		Dict_delete_iterator(iter);
		return true;
}

#ifdef DEBUG
void Dict_test(void) {
	u8 ret;

	Dict test_dict = Dict_new();

	Dict_new_row(test_dict, "key 1", sizeof("key 1"), "value 1", sizeof("value 1"));
	Dict_new_row(test_dict, "key 2", sizeof("key 2"), "value 2", sizeof("value 2"));
	Dict_new_row(test_dict, "key 3", sizeof("key 3"), "value 3", sizeof("value 3"));

	Dict_print(test_dict);


	ret = Dict_del_row(test_dict, "key 2", sizeof("key 3"));
	if (ret) {
		puts("record not found");
		return;
	}

	Dict_print(test_dict);

	Dict_new_row(test_dict, "key 1", sizeof("key 1"), "value 1", sizeof("value 1"));

	Dict_print(test_dict);

	ret = Dict_del_row(test_dict, "key 2", sizeof("key 3"));

	Dict_print(test_dict);
	Dict_new_row(test_dict, "key 4", sizeof("key 1"), "value 1", sizeof("value 1"));
	Dict_print(test_dict);
}
#endif


//--------------------------------------------------------
//	Lists
//--------------------------------------------------------

// Struct represents one node of list
struct ListNodeStruct {
	ListNode prev;
	ListNode next;
	void *data;
	unsigned short size;
};

// Struct represents one list
struct ListHeader {
	ListNode head;
	ListNode last;
	unsigned short total_nodes;
};

// Struct represents iteration for lists
struct ListIteratorStruct {
	List list;
	ListNode current;
};

/**
  * @brief  List List_new(void)
  * 		========== Creates new list
  * @retval return: struct List
  * 	- New list struct
*/
List List_new(void) {
	List list = malloc(sizeof(struct ListHeader));

	// Init free dict
	list->total_nodes = 0;
	list->head = list->last = NULL;

	return list;
}
/**
* @brief  bool List_is_empty(List list)
* 		========== Checks is list is empty
* @param  list: struct List
* @retval return: {True, False}
* 	- Returns True when list is empty, overwise False.
*/
bool List_is_empty(List list) { return list->total_nodes == 0; }

/**
* @brief  void List_append(List list, void *data, unsigned short size)
* 		========== Appends new elements to the list
* @param  list: struct List
* 	- Structure of the list where you want to add new element
* @param  *data: void[x]
* 	- Pointer to data that must be append to list element
* @param  *size: {1-65535}
* 	- Size of appending data
* @retval none
*/
void List_append(List list, void *data, unsigned short size) {
	ListNode new_node = malloc(sizeof(struct ListNodeStruct));

	new_node->data = malloc(size);
	new_node->next = NULL;
	new_node->size = size;

	memcpy(new_node->data, data, size);

	if (list->total_nodes == 0) {
		list->head = list->last = new_node;
		new_node->prev = NULL;
	} else {
		new_node->prev = list->last;
		list->last->next = new_node;
		list->last = new_node;
	}

	list->total_nodes++;
}

/**
* @brief  u8 List_pop(List list, void *data, unsigned short size)
* 		========== Pops first element from list
* @param  list: struct List
* 	- Structure of the list where you want pops element
* @param  *size: {1-65535}
* 	- Size of retrieving data
* @param *data: void[x]
* 	- Pointer to free memory space for retrieving data from list element
* @retval return: { 0, 1}
* 	- Returns 0 when retrieving data was successful, when 1 list is empty
*/
u8 List_pop(List list, void *data, unsigned short size) {
	if (list->total_nodes == 0)
		return 1;

	ListNode node = list->head;

	memcpy(data, node->data, size);
	list->head = list->head->next;

	list->total_nodes--;

	free(node->data);
	free(node);
	return 0;
}

/**
* @brief  ListIterator List_new_iterator(List list)
* 		========== Creates new iterator for retrieving elements from list
* @param  list: struct ListHeader
* 	- List for iteration
* @retval return: struct ListIteraror
* 	- New list iterator
*/
ListIterator List_new_iterator(List list) {
	ListIterator iter = malloc(sizeof(struct ListIteratorStruct));
	iter->list = list;
	iter->current = list->head;
	return iter;
}

/**
* @brief  void List_delete_iterator(ListIterator iter)
* 		========== Deletes desired list iterator
* @param  iter: struct ListIterator
* 	- List iterator for deleting
* @retval none
*/
void List_delete_iterator(ListIterator iter) { free(iter); }

/**
* @brief  ListNode List_iterate(ListIterator iter)
* 		========== Iterates desired iterator and returns elements from list
* @param  iter: struct ListIterator
* 	- List iterator for iteration
* @retval return: struct ListNode
* 	- Returns next element from list. Returns NULL when iteration was finished or when list is empty.
* 		When you try to iterate it's again iteration spend from beginning.
*/
ListNode List_iterate(ListIterator iter, void *data) {
	if (iter->current == NULL) {
		//reset iterator to first position and return NULL pointer
		iter->current = iter->list->head;
		return NULL;
	}

	ListNode node = iter->current;
	iter->current = iter->current->next;

	if (data != NULL)
		memcpy(data, node->data, node->size);
	return node;
}

/**
* @brief  u8 List_del_node(List list, ListNode node)
* 		========== Deletes node from list
* @param  list: struct ListHeader
* 	- List thats node must be removed
* @param  node: struct LostNode
* 	- Node thats must be removed
* @retval none
*/
void List_del_node(List list, ListNode node) {
	if (node == NULL) return;

	free(node->data);
	//when row is single
	if (list->total_nodes == 1) {
		list->head = list->last = NULL;
	} else
	//when row is first
	if (node->prev == NULL) {
		list->head = node->next;
		list->head->prev = NULL;
	} else
	//when row is last
	if (node->next == NULL) {
		list->last = node->prev;
		list->last->next = NULL;
	} else {
		// when row in middle of table
		node->prev->next = node->next;
		node->next->prev = node->prev;
	}
	free(node);
	list->total_nodes--;
}

#ifdef DEBUG
void List_test() {
	List list = List_new();
	u8 pos = 0;

	List_append(list, "node 1", sizeof("node 1"));
	List_append(list, "node 2", sizeof("node 1"));
	List_append(list, "node 3", sizeof("node 1"));

	void *buf = malloc(sizeof("node 1"));

	puts("\n--Elements list--");
	puts("|POS|	NODE 	|");

//	List_pop(list, buf, sizeof("node 1"));
//
//	List_append(list, "new", sizeof("new"));
//	List_pop(list, buf, sizeof("node 1"));
//
//	List_append(list, "new1", sizeof("new"));

	while (!List_pop(list, buf, sizeof("node 1"))) {
		printf("  %i", pos);
		printf("\t%s\n", buf);
		pos++;
	}
}
#endif




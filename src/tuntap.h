/**
  ******************************************************************************
  * @file    tuntap.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    22/11/2014
  * @brief   This file contains global variables and function prototypes for
  * 		 TUN network interface management
  ******************************************************************************
**/

#ifndef TUNTAP_H_
#define TUNTAP_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */

/* Library includes. */

/* Headers includes. */
#include "misc.h"

/* Macros --------------------------------------------------------------------*/
#define MTU	1280

/* Global Variables -----------------------------------------------------------*/
int tapdevice_fd;

/* Function Prototypes --------------------------------------------------------*/
void newaddr6(byte *, byte *);
int tun_alloc(char *);
void tapdevice_read(byte *);
void tapdevice_write(byte *);

#endif /* TUNTAP_H_ */

/**
  ******************************************************************************
  * @file    queue.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    14/05/2015
  * @brief   Provides queues for manet
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <string.h>
#include <time.h>
#include <stdlib.h>

/* Library includes. */

/* Headers includes. */
#include "misc.h"
#include "queue.h"
#include "node.h"
#include "dynamic.h"

/* Macros --------------------------------------------------------------------*/
#define LIMBO_LIFETIME 30

/* Function Prototypes --------------------------------------------------------*/

/* Global Variables =----------------------------------------------------------*/
List Queue;
struct queue_node {
	bool need_err;
	byte pkt[512];
	byte src[8];
	byte dst[8];
	u8 priority;
	time_t time;
};

List Limbo;
struct limbo_node {
	byte parcel_id[8];
	time_t time;
	struct {
		bool need_err;
		byte pkt[512];
		byte src[8];
		byte dst[8];
		u8 priority;
		time_t time;
	} parcel;
};

/**
  * @brief	void Queue_init(void)
  * 		========== Inits queue variables
  *	@param	none
  * @retval none
*/
void Queue_init(void) {
	Limbo = List_new();
	Queue = List_new();
}

/**
  * @brief	void Queue_push(bool need_err, byte *pkt, byte *src, byte *dst, u8 priority)
  * 		==========Pushes packet into queue
  *	@param	tx_need_err: {True, False}
  * @param  *pkt: byte[x+2], byte[0:1] is message size
  * 	- Compressed IP packet
  * @param  *tx_src: byte[8]
  * 	- 64bit L2 source address
  * @param  *tx_dst: byte[8]
  * 	- 64bit L2 source address
  *	@param priority: {ROUTING_PRIORITY, URGENT_PRIORITY, FORWARDING_PRIORITY, MY_PRIORITY}
  *		- Sets priority of L2 packet
  * @retval none
*/
void Queue_push(bool need_err, byte *pkt, byte *src, byte *dst, u8 priority) {
	struct queue_node *q = malloc(sizeof(struct queue_node));

	q->need_err = need_err;
	byteinit(q->pkt);
	byteadd(q->pkt, pkt);
	memcpy(q->src, src, 8);
	memcpy(q->dst, dst, 8);
	q->priority = priority;
	q->time = time(NULL);

	List_append(Queue, q, sizeof(struct queue_node));
	free(q);
}

/**
  * @brief	void Limbo_push(bool need_err, byte *pkt, byte *src, byte *dst, u8 priority, byte *parcel_id)
  * 		==========Pushes packet into limbo
  *	@param	tx_need_err: {True, False}
  * @param  *pkt: byte[x+2], byte[0:1] is message size
  * 	- Compressed IP packet
  * @param  *tx_src: byte[8]
  * 	- 64bit L2 source address
  * @param  *tx_dst: byte[8]
  * 	- 64bit L2 source address
  *	@param priority: {ROUTING_PRIORITY, URGENT_PRIORITY, FORWARDING_PRIORITY, MY_PRIORITY}
  *		- Sets priority of L2 packet
  * @param  *parcel_id: byte[8]
  * 	- Address for waiting in limbo
  * @retval none
*/
void Limbo_push(bool need_err, byte *pkt, byte *src, byte *dst, u8 priority, byte *parcel_id) {
	struct limbo_node *l = malloc(sizeof(struct limbo_node));

	l->parcel.need_err = need_err;
	byteinit(l->parcel.pkt);
	byteadd(l->parcel.pkt, pkt);
	memcpy(l->parcel.src, src, 8);
	memcpy(l->parcel.dst, dst, 8);
	l->parcel.priority = priority;
	l->parcel.time = time(NULL);

	memcpy(l->parcel_id, parcel_id, 8);
	l->time = time(NULL);

	List_append(Limbo, l, sizeof(struct limbo_node));
	free(l);
}

/**
  * @brief	void Queue_pull(bool *need_err, byte *pkt, byte *src, byte *dst, u8 *priority)
  * 		========== Pulls next packet from queue
  *	@retval	tx_need_err: {True, False}
  * @retval  *pkt: byte[x+2], byte[0:1] is message size
  * 	- Compressed IP packet
  * @retval  *tx_src: byte[8]
  * 	- 64bit L2 source address
  * @retval  *tx_dst: byte[8]
  * 	- 64bit L2 source address
  *	@retval priority: {ROUTING_PRIORITY, URGENT_PRIORITY, FORWARDING_PRIORITY, MY_PRIORITY}
  *		- Sets priority of L2 packet
*/
void Queue_pull(bool *need_err, byte *pkt, byte *src, byte *dst, u8 *priority) {
	struct queue_node *q =  malloc(sizeof(struct queue_node));

	List_pop(Queue, q, sizeof(struct queue_node));
	*need_err = q->need_err;
	byteinit(pkt);
	byteadd(pkt, q->pkt);
	memcpy(src, q->src, 8);
	memcpy(dst, q->dst, 8);
	*priority = q->priority;

	free(q);
}

/**
  * @brief	void Limbo_pull(byte *parcel_id)
  * 		========== Pulls next packet from limbo and pushes it to queue for sending
  * @param  *parcel_id: byte[8]
  * 	- Address for waiting in limbo
  *	@retval none
*/
void Limbo_pull(byte *parcel_id) {
	struct limbo_node *l = malloc(sizeof(struct limbo_node));

	ListIterator iter = List_new_iterator(Limbo);
	ListNode node = List_iterate(iter, l);

	while (node != NULL) {
		if (l->time && bytecomp(l->parcel_id, parcel_id, 8)) {
			Queue_push(l->parcel.need_err, l->parcel.pkt, l->parcel.src, l->parcel.dst, l->parcel.priority);
			List_del_node(Limbo, node);
		}
		node = List_iterate(iter, l);
	}
	free(l);
	List_delete_iterator(iter);
}

/**
  * @brief void Queue_clean(time_t currtime)
  * 		========== Clean out of date queue positions
  *	@param  currtime: seconds from new era
  *		- Current time
  * @retval none
*/
void Queue_clean(time_t currtime) {
	struct queue_node *q = malloc(sizeof(struct queue_node));

	ListIterator iter = List_new_iterator(Queue);
	ListNode node = List_iterate(iter, q);

	while (node != NULL) {
		if (q->time && (currtime - q->time > PACKET_LIFETIME)) {
			List_del_node(Queue, node);
		}
		node = List_iterate(iter, q);
	}
	free(q);
	List_delete_iterator(iter);
}

/**
  * @brief void Limbo_clean(time_t currtime)
  * 		========== Clean out of date limbo queue positions
  *	@param  currtime: seconds from new era
  *		- Current time
  * @retval none
*/
void Limbo_clean(time_t currtime) {
	struct limbo_node *l = malloc(sizeof(struct limbo_node));

	ListIterator iter = List_new_iterator(Limbo);
	ListNode node = List_iterate(iter, l);

	while (node != NULL) {
		if (l->time && (currtime - l->time > LIMBO_LIFETIME)) {
			List_del_node(Limbo, node);
		}
		node = List_iterate(iter, l);
	}
	free(l);
	List_delete_iterator(iter);
}

/**
  * @brief	bool Queue_empty(void)
  * 		========== Checks whether the queue is empty
  *	@retval return: {True, False}
  *		- True when queue is empty. False overwise.
*/
bool Queue_empty(void) {
	return List_is_empty(Queue);
}

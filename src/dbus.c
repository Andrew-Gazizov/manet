/**
  ******************************************************************************
  * @file    dbus.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    23/05/2015
  * @brief   Provides DBus routines for node and radio control
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <dbus/dbus.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

/* Library includes. */

/* Headers includes. */
#include "misc.h"
#include "chip_master.h"
#include "manet.h"

/* Macros --------------------------------------------------------------------*/
// DBus methods
#define NODE_MODE		0
#define RADIO_CHANNEL	1
#define RADIO_MODE		2
#define RADIO_POWER		3

#define FREE			255		// when no methods for processing

/* Variables -----------------------------------------------------------------*/
u8 dbus_status;			// Current DBus processing method

DBusConnection* conn;
DBusMessage* msg;

/* Function Prototypes -------------------------------------------------------*/

/**
  * @brief	void dbus_method_reply(char *answer)
  * 		========== Replies for requested DBus method
  * @param  *answer: char[x]
  * 	- Strings, contains answer
  * @retval none
*/
void dbus_method_reply(char *answer)
{
	printf("dbus_method_reply %d\n", dbus_status);
	if (dbus_status == FREE)
		return;

	DBusMessage* reply;
	DBusMessageIter args;
	dbus_uint32_t serial = 0;

	// create a reply from the message
	reply = dbus_message_new_method_return(msg);

	// add the arguments to the reply
	dbus_message_iter_init_append(reply, &args);
	if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &answer)) {
		fprintf(stderr, "Out Of Memory!\n");
		exit(1);
	}

	// send the reply && flush the connection
	if (!dbus_connection_send(conn, reply, &serial)) {
		fprintf(stderr, "Out Of Memory!\n");
		exit(1);
	}
	dbus_connection_flush(conn);

	// free the reply
	dbus_message_unref(reply);
	dbus_status = FREE;
}

/**
  * @brief	void dbus_process_input(u8 in_type)
  * 		========== Processing DBus method
  * @param  in_type: {0-254}
  * 	- DBus method type, list of available methods see above in Macros section
  * @retval none
*/
void dbus_process_input(u8 in_type)
{
	DBusMessageIter args;
	char *param;

	if (!dbus_message_iter_init(msg, &args)) {
		fprintf(stderr, "Message has no arguments!\n");
		return;
	}

	if (DBUS_TYPE_STRING != dbus_message_iter_get_arg_type(&args))
		fprintf(stderr, "Argument is not string!\n");
	else
		dbus_message_iter_get_basic(&args, &param);

	switch (in_type) {
	case RADIO_MODE:
		// read the arguments
		printf("Method RADIO_MODE called with %s\n", param);
		dbus_status = RADIO_MODE;
		if (strcmp(param, "off") == 0)
			turn_off_radio();
		else if (strcmp(param, "on") == 0)
			turn_on_radio();
		else if (strcmp(param, "?") == 0)
			get_radio_mode();
		else {
			printf("Bad NODE_MODE param %s\n", param);
			dbus_status = FREE;
			return;
		}
		break;
	case NODE_MODE:
		// read the arguments
		printf("Method NODE_MODE called with %s\n", param);
		dbus_status = NODE_MODE;
		if (strcmp(param, "run") == 0) {
			cfg.node_status = RUN;
			turn_on_radio();
		} else if (strcmp(param, "suspend") == 0) {
			cfg.node_status = SUSPEND;
			usleep(100000);
			turn_off_radio();
		} else if (strcmp(param, "?") == 0) {
			if (cfg.node_status == RUN)
				dbus_method_reply("run");
			else if (cfg.node_status == RUN)
				dbus_method_reply("suspend");
			else
				puts("Invalid node status value.");
		}
			printf("Bad RADIO_MODE param %s\n", param);
			dbus_status = FREE;
			return;
		break;
	case RADIO_CHANNEL:
		// read the arguments
		printf("Method RADIO_CHANNEL called with %s\n", param);
		dbus_status = RADIO_CHANNEL;
		cfg.channel = atoi(param);
		restore_settings();
		dbus_status = FREE;
		dbus_method_reply(param);
		break;
	}
}

/**
  * @brief	void dbus_thread(void)
  * 		========== Main DBus thread, monitoring DBus requests and processing it
  * @param  none
  * @retval none
*/
void dbus_thread(void) {
	// loop, testing for new messages
	while (true) {
		// non blocking read of the next available message
		dbus_connection_read_write(conn, 0);
		msg = dbus_connection_pop_message(conn);

		// loop again if we haven't got a message
		if (NULL == msg) {
			sleep(1);
			continue;
		}

		// check this is a method call for the right interface & method
		if (dbus_message_is_method_call(msg, "com.manet.node", "Mode"))
			dbus_process_input(NODE_MODE);
		else if (dbus_message_is_method_call(msg, "com.manet.radio", "Channel"))
			dbus_process_input(RADIO_CHANNEL);
		else if (dbus_message_is_method_call(msg, "com.manet.radio", "Mode"))
			dbus_process_input(RADIO_MODE);
		else if (dbus_message_is_method_call(msg, "com.manet.radio", "Power"))
			dbus_process_input(RADIO_POWER);

		// free the message
		dbus_message_unref(msg);
	}
}

/**
  * @brief	void dbus_init(void)
  * 		========== Inits DBus server and creating main DBus thread
  * @param  none
  * @retval none
*/
void dbus_init(void)
{
	pthread_t thread;
	DBusError err;
	int ret;

	dbus_status = FREE;
	printf("Listening for method calls\n");

	// initialise the error
	dbus_error_init(&err);

	// connect to the bus and check for errors
	conn = dbus_bus_get(DBUS_BUS_SESSION, &err);
	if (dbus_error_is_set(&err)) {
		fprintf(stderr, "Connection Error (%s)\n", err.message);
		dbus_error_free(&err);
	}
	if (NULL == conn) {
		fprintf(stderr, "Connection Null\n");
		exit(1);
	}

	// request our name on the bus and check for errors
	ret = dbus_bus_request_name(conn, "com.manet.server", DBUS_NAME_FLAG_REPLACE_EXISTING , &err);
	if (dbus_error_is_set(&err)) {
		fprintf(stderr, "Name Error (%s)\n", err.message);
		dbus_error_free(&err);
	}
	if (DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER != ret) {
		fprintf(stderr, "Not Primary Owner (%d)\n", ret);
		exit(1);
	}

	ret = pthread_create(&thread, NULL, dbus_thread, NULL);
	if (ret < 0) {
		perror("failed to create DBus thread");
	}
}


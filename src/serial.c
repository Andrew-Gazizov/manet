#include "serial.h"

struct termios new_port_settings,
       old_port_settings[30];

int Cport[30],
    error;

int RS232_OpenComport(int fd, const char *comport, int baudrate, const char *mode)
{
  int baudr, status;

  switch(baudrate) {
    case      50 : baudr = B50;
                   break;
    case      75 : baudr = B75;
                   break;
    case     110 : baudr = B110;
                   break;
    case     134 : baudr = B134;
                   break;
    case     150 : baudr = B150;
                   break;
    case     200 : baudr = B200;
                   break;
    case     300 : baudr = B300;
                   break;
    case     600 : baudr = B600;
                   break;
    case    1200 : baudr = B1200;
                   break;
    case    1800 : baudr = B1800;
                   break;
    case    2400 : baudr = B2400;
                   break;
    case    4800 : baudr = B4800;
                   break;
    case    9600 : baudr = B9600;
                   break;
    case   19200 : baudr = B19200;
                   break;
    case   38400 : baudr = B38400;
                   break;
    case   57600 : baudr = B57600;
                   break;
    case  115200 : baudr = B115200;
                   break;
    case  230400 : baudr = B230400;
                   break;
    case  460800 : baudr = B460800;
                   break;
    case  500000 : baudr = B500000;
                   break;
    case  576000 : baudr = B576000;
                   break;
    case  921600 : baudr = B921600;
                   break;
    case 1000000 : baudr = B1000000;
                   break;
    case 1152000 : baudr = B1152000;
                   break;
    case 1500000 : baudr = B1500000;
                   break;
    case 2000000 : baudr = B2000000;
                   break;
    case 2500000 : baudr = B2500000;
                   break;
    case 3000000 : baudr = B3000000;
                   break;
    case 3500000 : baudr = B3500000;
                   break;
    case 4000000 : baudr = B4000000;
                   break;
    default      : printf("invalid baudrate\n");
                   return(1);
                   break;
  }

  int cbits = CS8, cpar = IGNPAR, bstop = 0;

  if(strlen(mode) != 3) {
    printf("invalid mode \"%s\"\n", mode);
    return(1);
  }

  switch(mode[0]) {
    case '8': cbits = CS8;
              break;
    case '7': cbits = CS7;
              break;
    case '6': cbits = CS6;
              break;
    case '5': cbits = CS5;
              break;
    default : printf("invalid number of data-bits '%c'\n", mode[0]);
              return(1);
              break;
  }

  switch(mode[1]) {
    case 'N':
    case 'n': cpar = IGNPAR;
              break;
    case 'E':
    case 'e': cpar = PARENB;
              break;
    case 'O':
    case 'o': cpar = (PARENB | PARODD);
              break;
    default : printf("invalid parity '%c'\n", mode[1]);
              return(1);
              break;
  }

  switch(mode[2]) {
    case '1': bstop = 0;
              break;
    case '2': bstop = CSTOPB;
              break;
    default : printf("invalid number of stop bits '%c'\n", mode[2]);
              return(1);
              break;
  }

  fd = open(comport, O_RDWR | O_NOCTTY | O_NDELAY);
  serial_fd = fd;

  if(fd == -1) {
    perror("unable to open comport ");
    return(1);
  }

  error = tcgetattr(fd, old_port_settings + fd);
  if(error == -1) {
    close(fd);
    perror("unable to read portsettings ");
    return(1);
  }
  memset(&new_port_settings, 0, sizeof(new_port_settings));  /* clear the new struct */

  new_port_settings.c_cflag = cbits | cpar | bstop | CLOCAL | CREAD;
  new_port_settings.c_iflag = IGNPAR;
  new_port_settings.c_oflag = 0;
  new_port_settings.c_lflag = 0;
  new_port_settings.c_cc[VMIN] = 0;      /* block untill n bytes are received */
  new_port_settings.c_cc[VTIME] = 0;     /* block untill a timer expires (n * 100 mSec.) */

  cfsetispeed(&new_port_settings, baudr);
  cfsetospeed(&new_port_settings, baudr);

  error = tcsetattr(fd, TCSANOW, &new_port_settings);
  if(error == -1) {
    close(fd);
    perror("unable to adjust portsettings ");
    return(1);
  }

  if(ioctl(fd, TIOCMGET, &status) == -1) {
    perror("unable to get portstatus");
    return(1);
  }

  status |= TIOCM_DTR;    /* turn on DTR */
  status |= TIOCM_RTS;    /* turn on RTS */

  if(ioctl(fd, TIOCMSET, &status) == -1) {
    perror("unable to set portstatus");
    return(1);
  }


  return(0);
}


unsigned short RS232_PollComport(int fd, unsigned char *buf, unsigned short size)
{
  unsigned short n;

  n = read(fd, buf, size);
  return(n);
}


int RS232_SendByte(int fd, unsigned char byte)
{
  int n;

  n = write(fd, &byte, 1);
  if(n<0)  return(1);

  return(0);
}


int RS232_SendBuf(int fd, unsigned char *buf, int size)
{
  return(write(fd, buf, size));
}


void RS232_CloseComport(int fd)
{
  int status;

  if(ioctl(fd, TIOCMGET, &status) == -1)
  {
    perror("unable to get portstatus");
  }

  status &= ~TIOCM_DTR;    /* turn off DTR */
  status &= ~TIOCM_RTS;    /* turn off RTS */

  if(ioctl(fd, TIOCMSET, &status) == -1)
  {
    perror("unable to set portstatus");
  }

  tcsetattr(fd, TCSANOW, old_port_settings + fd);
  close(fd);
}

/*
Constant  Description
TIOCM_LE        DSR (data set ready/line enable)
TIOCM_DTR       DTR (data terminal ready)
TIOCM_RTS       RTS (request to send)
TIOCM_ST        Secondary TXD (transmit)
TIOCM_SR        Secondary RXD (receive)
TIOCM_CTS       CTS (clear to send)
TIOCM_CAR       DCD (data carrier detect)
TIOCM_CD        see TIOCM_CAR
TIOCM_RNG       RNG (ring)
TIOCM_RI        see TIOCM_RNG
TIOCM_DSR       DSR (data set ready)
*/

int RS232_IsDCDEnabled(int fd)
{
  int status;

  ioctl(fd, TIOCMGET, &status);

  if(status&TIOCM_CAR) return(1);
  else return(0);
}

int RS232_IsCTSEnabled(int fd)
{
  int status;

  ioctl(fd, TIOCMGET, &status);

  if(status&TIOCM_CTS) return(1);
  else return(0);
}

int RS232_IsDSREnabled(int fd)
{
  int status;

  ioctl(fd, TIOCMGET, &status);

  if(status&TIOCM_DSR) return(1);
  else return(0);
}

void RS232_enableDTR(int fd)
{
  int status;

  if(ioctl(fd, TIOCMGET, &status) == -1)
  {
    perror("unable to get portstatus");
  }

  status |= TIOCM_DTR;    /* turn on DTR */

  if(ioctl(fd, TIOCMSET, &status) == -1)
  {
    perror("unable to set portstatus");
  }
}

void RS232_disableDTR(int fd)
{
  int status;

  if(ioctl(fd, TIOCMGET, &status) == -1)
  {
    perror("unable to get portstatus");
  }

  status &= ~TIOCM_DTR;    /* turn off DTR */

  if(ioctl(fd, TIOCMSET, &status) == -1)
  {
    perror("unable to set portstatus");
  }
}

void RS232_enableRTS(int fd)
{
  int status;

  if(ioctl(fd, TIOCMGET, &status) == -1)
  {
    perror("unable to get portstatus");
  }

  status |= TIOCM_RTS;    /* turn on RTS */

  if(ioctl(fd, TIOCMSET, &status) == -1)
  {
    perror("unable to set portstatus");
  }
}

void RS232_disableRTS(int fd)
{
  int status;

  if(ioctl(fd, TIOCMGET, &status) == -1)
  {
    perror("unable to get portstatus");
  }

  status &= ~TIOCM_RTS;    /* turn off RTS */

  if(ioctl(fd, TIOCMSET, &status) == -1)
  {
    perror("unable to set portstatus");
  }
}

void RS232_cputs(int fd, const char *text)  /* sends a string to serial port */
{
  while(*text != 0)   RS232_SendByte(fd, *(text++));
}



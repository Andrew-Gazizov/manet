/**
  ******************************************************************************
  * @file    logger.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    02/07/2015
  * @brief   This file contains global variables and function prototypes for
  * 		 logger
  ******************************************************************************
**/

#ifndef LOGGER_H_
#define LOGGER_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
/* Library includes. */
/* Headers includes. */

/* Macros --------------------------------------------------------------------*/

/* Global Variables ----------------------------------------------------------*/
typedef enum {
	DEBUG,
	INFO,
	WARNING,
	ERROR,
	SILENT
} log_level_t;

log_level_t __g_loglevel;

/* Function Prototypes -------------------------------------------------------*/
void log_init(void);
void LOG_TRACE(log_level_t level, const char *format, ...);


#endif /* LOGGER_H_ */

/**
  ******************************************************************************
  * @file    sslip.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    25/11/2014
  * @brief   This file contains function prototypes for processing input chip
  * 			serial data
  ******************************************************************************
**/

#ifndef SSLIP_H_
#define SSLIP_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */

/* Library includes. */

/* Headers includes. */
#include "misc.h"

/* Macros --------------------------------------------------------------------*/

/* Function Prototypes --------------------------------------------------------*/
u8 sslip_find_data_pos(byte *, u8, byte);
unsigned short sslip_find_cmd_pos(byte *, u8);

#endif /* SSLIP_H_ */

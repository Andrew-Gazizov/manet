/**
  ******************************************************************************
  * @file    logger.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    02/07/2015
  * @brief   Provides logging with log levels
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>

/* Library includes. */

/* Headers includes. */
#include "logger.h"
#include "node.h"

/* Macros --------------------------------------------------------------------*/
#define MSG_SIZE 256

/* Variables -----------------------------------------------------------------*/
FILE	    *__g_loghandle;

/* Function Prototypes -------------------------------------------------------*/

/**
  * @brief	void log_init(void)
  * 		========== Inits logger and sets current log level
  * @param  none
  * @retval none
*/
void log_init(void) {
	__g_loghandle = stdout;
}

/**
  * @brief	void LOG_TRACE(log_level_t level, const char *format, ...)
  * 		========== Prints formatted message in stdout
  * @param  level: {2-65535}
  * 	- Num of bytes for swapping
  * @param  *format: const char
  * 	- formatted string for printing
  * @param  num: {DEBUG, WARNING, INFO, ERROR}
  * 	- Log level for message
  * @retval none
*/
void LOG_TRACE(log_level_t level, const char *format, ...) {
 	if(level >= __g_loglevel) {
 		char *slevel;
 	 	va_list ap;
 	 	time_t 	 rawtime;
 	   	struct tm * timeinfo;

 		char *buffer = malloc(MSG_SIZE);
 		malloc_count++;
 		char *timestamp = malloc(MSG_SIZE);
 		malloc_count++;

 		va_start(ap, format);
 		vsnprintf(buffer, MSG_SIZE, format, ap);
 		va_end(ap);
 		time(&rawtime);
   		timeinfo = localtime(&rawtime);

   		strftime(timestamp, MSG_SIZE, "%d/%m/%Y %X", timeinfo);

 		switch(level) {
 			case DEBUG   : slevel = "DEBUG"; break;
 			case WARNING : slevel = "WARNING"; break;
 			case INFO    : slevel = "INFO"; break;
 			case ERROR   : slevel = "ERROR"; break;
 			case SILENT  : return;
 		}
 		fprintf(stdout, "[%s] [%s] %s\n", timestamp, slevel, (const char *)buffer );
 		fflush(stdout);

 		free(buffer);
 		malloc_count--;
 		free(timestamp);
 		malloc_count--;
     }
 }



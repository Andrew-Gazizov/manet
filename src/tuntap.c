/**
  ******************************************************************************
  * @file    tuntap.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    22/11/2014
  * @brief   TUN network interface management routines
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <sys/socket.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* Library includes. */

/* Headers includes. */
#include "tuntap.h"
#include "logger.h"

/* Macros --------------------------------------------------------------------*/

/* Variables -----------------------------------------------------------------*/

/* Function Prototypes -------------------------------------------------------*/
void tapdevice_up(byte *);

/**
  * @brief	int tun_alloc(char *dev)
  * 		========== Creates new TUN network interface
  * @param  *dev: byte[x]
  * 	- TUN interface name
  * @retval return: {0-2}
  * 	- 0 when interface is created. Overwise error occured.
*/
int tun_alloc(char *dev) {
	struct ifreq ifr;
    int err;
    byte str[32];
    byte mtu[8];

    if( (tapdevice_fd = open("/dev/net/tun", O_RDWR)) < 0 ) return -1;

    memset(&ifr, 0, sizeof(ifr));

    /* Flags: IFF_TUN   - TUN device (no Ethernet headers)
    *        IFF_TAP   - TAP device
    *
    *        IFF_NO_PI - Do not provide packet information
    */
    ifr.ifr_flags = IFF_TUN | IFF_NO_PI;
    if( *dev )
    	strncpy(ifr.ifr_name, dev, IFNAMSIZ);

    if ( (err = ioctl(tapdevice_fd, TUNSETIFF, (void *) &ifr)) < 0 ) {
    	close(tapdevice_fd);
        return err;
    }

    // Set proper MTU for new interface
    sprintf((char *)mtu, "%d", MTU);
    strcpy((char *)str, "ifconfig ");
    strcat((char *)str, (const char *)ifr.ifr_name);
    strcat((char *)str, " mtu ");
    strcat((char *)str, (const char *)mtu);
    system((char *)str);

    return 0;
 }

/**
  * @brief	void tapdevice_read(byte *data)
  * 		========== Read data from the device. The device mtu determines how many bytes
      	  	  	  	  	  will be read.
  * @retval  *data: byte[x+2], byte[0:1] is message size
  * 	- Read TUN data
*/
void tapdevice_read(byte *data) {
	unsigned short nread;

	nread = read(tapdevice_fd, &data[2], MTU);
	LOG_TRACE(DEBUG, "Tun read: %i bytes", nread);
	data[0] = nread & 0xFF;
	data[1] = (nread >> 8) & 0xFF;
}

/**
  * @brief	void tapdevice_write(byte *data)
  * 		==========  Write data to the device. No care is taken for MTU limitations or similar.
  * @param  *data: byte[x+2], byte[0:1] is message size
  * 	- Data for TUN write
  * @retval none
*/
void tapdevice_write(byte *data) {
	unsigned short nwrite;
	if((nwrite=write(tapdevice_fd, &data[2], (data[1] << 8) | data[0]))<0){
	    perror("Writing data");
	    exit(1);
	}
}

/**
  * @brief	void newaddr6(byte *ipaddr, byte *name)
  * 		==========  Adds new IPv6 address to network interface
  * @param  *name: byte[x]
  * 	- Network interface name
  * @param  *ipaddr: byte[8]
  * 	- IPv6 address
  * @retval none
*/
void newaddr6(byte *ipaddr, byte *name) {
	byte str[128];

	strcpy((char *)str, "ifconfig ");
	strcat((char *)str, (const char *)name);
	strcat((char *)str, " add ");
	strcat((char *)str, (const char *)ipaddr);
	strcat((char *)str, "/64");

	system((char *)str);
	tapdevice_up(name);
}

/**
  * @brief	void tapdevice_up(byte *name)
  * 		==========  Bring up device. This will effectively run "ifconfig up" on the device.
  * @param  *name: byte[x]
  * 	- Network interface name
  * @retval none
*/
void tapdevice_up(byte *name) {
	byte str[32];

	strcpy((char *)str, "ifconfig ");
	strcat((char *)str, (const char *)name);
	strcat((char *)str, " up");

	system((char *)str);
}

/**
  * @brief	void tapdevice_up(byte *name)
  * 		==========  Bring down device. This will effectively run "ifconfig down" on the device.
  * @param  *name: byte[x]
  * 	- Network interface name
  * @retval none
*/
void tapdevice_down(byte *name) {
	byte str[32];

	strcpy((char *)str, "ifconfig ");
	strcat((char *)str, (const char *)name);
	strcat((char *)str, " down");

	system((char *)str);
}



/**
  ******************************************************************************
  * @file    sixlowpan_defragmenter.c
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    22/11/2014
  * @brief   Main network program body.
  ******************************************************************************
**/

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <errno.h>
#include <time.h>
#include <stdlib.h>

/* Library includes. */

/* Headers includes. */
#include "node.h"
#include "misc.h"
#include "chip_master.h"
#include "tuntap.h"
#include "iptools.h"
#include "p802154.h"
#include "sixlowpan.h"
#include "load.h"
#include "sixlowpan_defragmenter.h"
#include "sixlowpan_compressor.h"
#include "manet.h"
#include "binascii.h"
#include "tuntap.h"
#include "queue.h"
#include "lan.h"
#include "dbus.h"
#include "logger.h"

/* Macros --------------------------------------------------------------------*/
#define CLEANING_PERIOD		1

#define LIFETIME_OF_L2_NEIGBOR	5
#define PACKET_LIFETIME 		5
#define MAX_L2_HEADER_SIZE		21
#define MAX_L2_PAYLOAD 			RADIO_FRAME_SIZE - MAX_L2_HEADER_SIZE

/* Variables -----------------------------------------------------------------*/
byte my_v6[16];

/* Function Prototypes -------------------------------------------------------*/
bool (*check_crc)(byte *);

/**
  * @brief	void Tun_Init(byte *iface)
  * 		========== Inits TUN, creating network interface and gets IPv6 address.
  * @param  *iface: byte[6]
  * 	- New network interface name
  * @retval none
*/
void Tun_Init(byte *iface) {
	tun_alloc((char *) iface);
	eui_to_ip(eui64, my_v6);
}

/**
  * @brief	void init6()
  * 		========== Adds new IPv6 address to network interface
  * @param none
  * @retval none
*/
void init6() {
	byte str[40];
	if (!inet_ntop(AF_INET6,my_v6, (char *) str, 40)) puts("ERROR: init6 error");
	newaddr6(str, (byte *) cfg.iface);
}

/**
  * @brief	void _write_to_outside(byte *msg)
  * 		========== Sends L2 packet to chip
  * @param  *msg: byte[x+2], byte[0:1] is message size
  * 	- L2 packet for sending
  * @retval none
*/
void _write_to_outside(byte *msg) {
	chip_write(msg);
}

/**
  * @brief	void _write_to_outside(byte *msg)
  * 		========== Sends L2 packet to chip or LAN
  * @param  *msg: byte[x+2], byte[0:1] is message size
  * 	- L2 packet for sending
  * @param  *l2dst: byte[8]
  * 	- Destination L2 address
  * @retval none
*/
void _lan_write_to_outside(byte *msg, byte *l2dst)
{
	if (L2toIP_is_in(l2dst, 0)) {
		LAN_write(msg, l2dst);
	}
	else {
		chip_write(msg);
		if (bytecomp(l2dst, BROADCAST16, 8))
			LAN_write_all(msg);
	}
}

/**
  * @brief	static inline bool check_crc_stm32w(byte *payload)
  * 		==========  CRC checking for STM32W no needed, so this is dummy inline function
  * @param  *pkt: byte[x+2], byte[0:1] is message size
  * 	- Input Packet
  * @retval return: True
*/
static inline bool check_crc_stm32w(byte *payload) { return true; }

/**
  * @brief 	static inline bool check_crc_cc430(byte *payload)
  * 		========== Checks packet CRC from CC430
  * @param  *pkt: byte[x+2], byte[0:1] is message size
  * 	- Input Packet
  * @retval return: {True, False}
  * 	- True when CRC is correct. Overwise false.
*/
static inline bool check_crc_cc430(byte *payload) {
	if (payload[((payload[1] << 8) | payload[0]) + 1] != 0x80) {
		LOG_TRACE(ERROR, "BAD CHIP CRC");
		return false;
	}
	payload[0]--;
	return true;
}

/**
  * @brief	void Node_Init()
  * 		========== Inits all program configuration
  * @param none
  * @retval none
*/
void Node_Init() {
	CONN_STATUS = 0;
	malloc_count = 0;
	if (cfg.lan)
		LAN_init(cfg.lan_iface);
	if (!strcmp(cfg.chip, "cc430")) check_crc = &check_crc_cc430;
	else check_crc = &check_crc_stm32w;
//	if (cfg.dbus)
//		dbus_init();
	ChipMaster_Init();
	Tun_Init((byte *) cfg.iface);
	init6();
	Queue_init();
	Router_init();
	sixlowpan_init();
	L2Master(eui64, LIFETIME_OF_L2_NEIGBOR);
	Defragmenter_init();
	complete_init();
}

/**
  * @brief	void process_data_from_outside(byte *payload)
  * 		========== Gets input lowpan packet from chip and it's processing. Retranslates it,
  * 		creates response or writes to TUN
  * @param  *msg: byte[x+2], byte[0:1] is message size
  * 	- Input lowpan packet
  * @retval none
*/
void process_data_from_outside(byte *payload) {
	// It's used in chip as callback

	if (!check_crc) return;

	char rssi = payload[((payload[1] << 8) | payload[0]) + 1];
	payload[0]--;
	if (rssi == 0) {
		LOG_TRACE(ERROR, "Zero RSSI value");
		return;
	}
	if (rssi != 10)	LOG_TRACE(DEBUG, "   <=== from radio");		// prints when received packet from radio

	if (l2unpack(payload) != SUCCESS) {
		LOG_TRACE(ERROR, "l2unpack error");
		return;
	}

	byte str[32];
	shexify(p802154.src, str, 8);
	LOG_TRACE(DEBUG, "L2 pkt from %s", str);

   	if (is_correct_not_lowpan(p802154.data)) {
   		LOG_TRACE(DEBUG, "Got LOAD pkt, type %i", p802154.data[0+2]);
		load_an(p802154.data, rssi);
		return;
	}

    Lowpan_init(p802154.data, p802154.src, p802154.dst);
	if (bytecomp(lowpan.src, eui64, 8)) {
		LOG_TRACE(DEBUG, "Drop packet from me");
		return;
	}

	byte *data = malloc(MTU+3);
	malloc_count++;
	byte *ip6_pkt = malloc(MTU+3);
	malloc_count++;

	byteinit(data);
	if (bytecomp(lowpan.dst, eui64, 8) || bytecomp(lowpan.dst, BROADCAST16, 8)) {
		if (!Lowpan_get_whole_payload(data)) goto out;
		if (is_compressed(data));
		// drops this packet and returns when decompress was unsuccessful
		if (!decompress(data, lowpan.hops_left, lowpan.src, lowpan.dst)) goto out;
		ip6_pack(ip6_pkt);
		tapdevice_write(ip6_pkt);
		LOG_TRACE(DEBUG, "   ===> packet to tun IPv6");
	}
	if (bytecomp(lowpan.dst, eui64, 8))
		goto out;
	if (lowpan.hops_left <= 1)
		goto out;
	lowpan.hops_left--;
	if (Router_has_route(lowpan.dst)) {
		Lowpan_serialize_for_retranslation(data);
		Queue_push(false, data, lowpan.src, lowpan.dst, FORWARDING_PRIORITY);
	} else {
		LOG_TRACE(DEBUG, "drop forwarded packet and sent RERR");
		Router_error(lowpan.dst, lowpan.src);
	}

out:
	free(data);
	malloc_count--;
	free(ip6_pkt);
	malloc_count--;
}

/**
  * @brief	void process_chip_bad_responce()
  * 		========== Processing chip error
  * @param none
  * @retval none
*/
void process_chip_bad_responce() {
	puts("ERROR: Chip bad response");
}

/**
  * @brief	void write_to_outside(bool tx_need_err, byte *msg, byte *tx_src, byte* tx_dst, u8 priority)
  * 		========== Forming L2 packet from queue and sends it's to chip or LAN
  *	@param none
  *		- Sets priority of L2 packet
  * @retval return: {True, False}
  * 	- Returns True when Queue is empty and nothing to send, overwise False
*/
bool write_to_outside(void) {
	if (Queue_empty()) return true;

	bool tx_need_err;
	byte tx_src[8];
	byte tx_dst[8];
	u8 priority;

	byte *m = malloc(256);
	malloc_count++;
	byte *msg = malloc(256);
	malloc_count++;

	Queue_pull(&tx_need_err, msg, tx_src, tx_dst, &priority);
	l2pack(tx_src, tx_dst, msg, (priority <= URGENT_PRIORITY), m);
	if (cfg.lan)
		_lan_write_to_outside(m, tx_dst);
	else
		_write_to_outside(m);

	free(m);
	malloc_count--;
	free(msg);
	malloc_count--;
	return false;
}

/**
  * @brief	void proccess_tun_data(byte *pkt_raw)
  * 		========== Processing IPv6 packet. Packing and send to chip.
  * @param  *pkt_raw: byte[x+2], byte[0:1] is message size
  * 	- IPv6 packet
  * @retval none
*/
void proccess_tun_data(byte *pkt_raw) {
	byte tx_dst[8];
	byte tx_src[8];
	u8 i = 0;
	bool has_route;

	ip6_init(pkt_raw);
	get_l2(ip6.dst, tx_dst);
	get_l2(ip6.src, tx_src);
	has_route = Router_has_route(tx_dst);

	if (!has_route) {
		Router_make_route(tx_dst);
	}
	LowpanGenerator(pkt_raw, MAX_L2_PAYLOAD, true, false);

	//FIXME: Fix this stupid construction!!!
	if (has_route) {
		if (fragments.fragments_num == 0) Queue_push(false, fragments.data[i], tx_src, tx_dst, URGENT_PRIORITY);
		else for (i = 0; i < fragments.fragments_num; i++) Queue_push(false, fragments.data[i], tx_src, tx_dst, URGENT_PRIORITY);
	} else {
		if (fragments.fragments_num == 0) Limbo_push(false, fragments.data[i], tx_src, tx_dst, URGENT_PRIORITY, tx_dst);
		else for (i = 0; i < fragments.fragments_num; i++)
			Limbo_push(false, fragments.data[i], tx_src, tx_dst, URGENT_PRIORITY, tx_dst);
	}
}

/**
  * @brief	void periodic_actions(void)
  * 		========== Processing periodic operations
  * @param none
  * @retval none
*/
void periodic_actions(void) {
	time_t ctime = time(NULL);
	Router_clean(ctime);
	sixlowpan_clean(ctime);
	Defragmenter_clean(ctime);
	L2Neighbors_clean(ctime);
	Queue_clean(ctime);
	Limbo_clean(ctime);
}

/**
  * @brief	void read_from_tun_and_process(void)
  * 		========== Reads IP packet from network interface and send it for processing
  * @param none
  * @retval none
*/
void read_from_tun_and_process(void) {
	byte *data = malloc(MTU+2);
	malloc_count++;

	byteinit(data);
	tapdevice_read(data);
	proccess_tun_data(data);

	free(data);
	malloc_count--;
}

/**
  * @brief	void node_run()
  * 		========== Main network function. Provides proper poll configuration and main network thread.
  * @param none
  * @retval none
*/
void node_run() {
	time_t next_cleaning_time = 0;

	const int NOF_FD = 3;
	int ser_tun_epoll;
    struct epoll_event ev;
    const int MAX_EVENTS = 1;
    int ready;

	if ((ser_tun_epoll = epoll_create(NOF_FD)) < 0) {
		perror("EPOLL CREATE failed");
		return;
	}

	memset(&ev, 0, sizeof(ev));
	ev.events = EPOLLIN;
	ev.data.fd = tapdevice_fd;
	if(epoll_ctl(ser_tun_epoll, EPOLL_CTL_ADD, tapdevice_fd, &ev) < 0) {
		perror("EPOLL CTL ADD fd_tun1 failed");
	    close(tapdevice_fd);
	    close(serial_fd);
	    close(ser_tun_epoll);
	    return;
	}

	memset(&ev, 0, sizeof(ev));
	ev.events = EPOLLIN;
	ev.data.fd = serial_fd;
	if(epoll_ctl(ser_tun_epoll, EPOLL_CTL_ADD, serial_fd, &ev) < 0) {
		perror("EPOLL CTL ADD fd_tun2 failed");
	    close(tapdevice_fd);
	    close(serial_fd);
	    close(ser_tun_epoll);
	    return;
	}

	if (cfg.lan) {
		memset(&ev, 0, sizeof(ev));
		ev.events = EPOLLIN;
		ev.data.fd = lan_skt;
		if(epoll_ctl(ser_tun_epoll, EPOLL_CTL_ADD, lan_skt, &ev) < 0) {
			perror("EPOLL CTL ADD lan_skt failed");
			close(tapdevice_fd);
			close(serial_fd);
			close(ser_tun_epoll);
			return;
		}
	}

	while (true) {
		if (next_cleaning_time < time(NULL)) {
			next_cleaning_time = time(NULL) + CLEANING_PERIOD;
			periodic_actions();
		}
		if((ready = epoll_wait(ser_tun_epoll, &ev, MAX_EVENTS, -1)) < 0) {
			if(errno == EINTR) continue;
			else {
				perror("EPOLL WAIT failed");
				close(tapdevice_fd);
				close(serial_fd);
				close(ser_tun_epoll);
				return;
			}
		}
		if ((cfg.node_status == RUN) & ev.events & EPOLLIN) {
			if (ev.data.fd == tapdevice_fd)
				read_from_tun_and_process();
			if (ev.data.fd == serial_fd)
				serial_reader();
			if (ev.data.fd == lan_skt)
				LAN_read_and_process();
		}

		while (is_idle() || is_timeout()) {
			if (write_to_outside())
				break;
		}

		if (malloc_count)
			printf("WARNING: Memory leaks found: %d\n", malloc_count);
	}
}

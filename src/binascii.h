/**
  ******************************************************************************
  * @file    binascii.h
  * @author  Andrew Gazizov
  * @version V1.0.0
  * @date    22/11/2014
  * @brief   This file contains global variables and function prototypes for
  * 		 converting binary strings to ASCII strings
  ******************************************************************************
**/

#ifndef BINASCII_H_
#define BINASCII_H_

/* Includes ------------------------------------------------------------------*/
/* Standard includes. */

/* Library includes. */

/* Headers includes. */
#include "misc.h"

/* Macros --------------------------------------------------------------------*/

/* Function Prototypes --------------------------------------------------------*/
void hexify(byte *, byte *, unsigned short);
void shexify(byte *, byte *, unsigned short);

#endif /* BINASCII_H_ */
